CREATE DATABASE gamika;

CREATE USER 'webapp'@'localhost' IDENTIFIED BY '0pl9ok8i';

CREATE TABLE gamika.events
  (
    id           INTEGER PRIMARY KEY AUTO_INCREMENT,
    name         VARCHAR (100) NOT NULL ,
    type         VARCHAR (20) NOT NULL ,
    players_limit INTEGER NOT NULL ,
    date_event    DATETIME NOT NULL ,
    city         VARCHAR (20) NOT NULL ,
    street       VARCHAR (100) ,
    game         VARCHAR (50) ,
    description   VARCHAR (500) ,
    user_login   VARCHAR (20) NOT NULL
  );

CREATE TABLE gamika.users
  (
    login 	VARCHAR (20) NOT NULL PRIMARY KEY,
    password BLOB NOT NULL ,
    salt 	BLOB NOT NULL ,
    email   VARCHAR (50) NOT NULL ,
    name    VARCHAR (50) ,
    surname VARCHAR (50) ,
    city    VARCHAR (100) ,
    photo 	BLOB
  );

CREATE TABLE gamika.usersevents
  (
    user_login VARCHAR (20) NOT NULL ,
    event_id   INTEGER NOT NULL
  ) ;
ALTER TABLE gamika.usersevents ADD CONSTRAINT relation_2_PK PRIMARY KEY ( user_login, event_id ) ;

ALTER TABLE gamika.events ADD CONSTRAINT event_user_FK FOREIGN KEY ( User_login ) REFERENCES gamika.users ( login ) ;

ALTER TABLE gamika.usersevents ADD CONSTRAINT FK_ASS_14 FOREIGN KEY ( user_login ) REFERENCES gamika.users ( login ) ;

ALTER TABLE gamika.usersevents ADD CONSTRAINT FK_ASS_15 FOREIGN KEY ( event_id ) REFERENCES gamika.events ( id ) ;

CREATE TABLE gamika.games (
  name 		VARCHAR(50) NOT NULL,
  rules 	VARCHAR(500) NULL,
  PEGI 		INT(2) UNSIGNED NULL,
  version 	INT(2) UNSIGNED NOT NULL,
  author 	VARCHAR(45) NULL,
  language 	VARCHAR(20) NULL,
  players_limit INT(2) NULL,
  type 		VARCHAR(20) NULL,
  PRIMARY KEY (name, version)
 );
  
 CREATE TABLE gamika.friends (
  date_friend INT NOT NULL,
  login1 	VARCHAR(20) NOT NULL,
  login2 	VARCHAR(20) NOT NULL,
  INDEX friend_idx (login1 ASC),
  INDEX friend2_idx (login2 ASC),
  CONSTRAINT friend1
	FOREIGN KEY (login1)
    REFERENCES gamika.users (login),
  CONSTRAINT friend2
    FOREIGN KEY (login2)
    REFERENCES gamika.users (login)
);


GRANT ALL ON gamika.* TO 'webapp'@'localhost';