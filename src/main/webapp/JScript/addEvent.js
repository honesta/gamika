$(document).ready( function() {
	var invalidEventName = true;
	var invalidPlayersLimit= true;
	var invalidDateEvent = false;
	var invalidCity = true;
	var invalidStreet = false;
	var invalidGameName = false;
	var invalidDescription = false;
	var invalidTime = false;
	$("#submit").css("display", "none");
	$(function() {
	    $( "#datepicker" ).datepicker($.datepicker.regional[ "pl" ]);
	    $("#timepicker").timepicker({ timeFormat: 'G:i' });
	  });
	function submitVisibility(){
		
		if(!invalidEventName  && !invalidPlayersLimit && !invalidDateEvent && !invalidCity && !invalidStreet && !invalidGameName && !invalidDescription) {
			$("#submit").css("display", "initial");
			}
		else {
			$("#submit").css("display", "none");
		}
	}

	$('#eventName').on('change keyup ',function() {
		if($(this).val().length < 4 || $(this).val().length > 30) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidEventName = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidEventName = false;
		}
		submitVisibility();
	});
	$('#timepicker').on('change keyup ',function() {
		
		if(!validateTime($(this).val())) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidTime = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidTime = false;
		}
		submitVisibility();
	}); 
	function validateTime(time) {
		var pattern = /^[0-9]{1,2}:[0-5]0/;
		return time.match(pattern);
	}
	$('#playersLimit').on('change keyup ',function() {
		var maxPlayers = parseInt($(this).val());
		if($(this).val().length < 1 || $(this).val().length > 5) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidPlayersLimit = true;
		}
		else if(isNaN(maxPlayers)) { 
			$(this).removeClass("validRegistration"); 
			$(this).addClass("invalidRegistration"); 
			invalidPlayersLimit = true; 
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidPlayersLimit = false;
		}
		submitVisibility();
	});
	
	$('#eventCity').on('change keyup ',function() {
		if($(this).val().length < 3 || $(this).val().length > 30) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidCity = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidCity = false;
		}
		submitVisibility();
	});
	
	$('#eventStreet').on('change keyup ',function() {
		if($(this).val().length > 30) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidStreet = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidStreet = false;
		}
		submitVisibility();
	});
	
	$('#gameName').on('change keyup ',function() {
		if($(this).val().length > 30) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidGameName = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidGameName = false;
		}
		submitVisibility();
	});
	
	$('#description').on('change keyup ',function() {
		if($(this).val().length > 150) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidDescription = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidDescription = false;
		}
		submitVisibility();
	});

	
	$('#submit').on('click', function() {
		var h = $('#timepicker').val();
		var dateTypeVar = $('#datepicker').datepicker('getDate');
		$.datepicker.formatDate('dd-mm-yy', dateTypeVar);
		var dateEvent = $("#datepicker").datepicker({ dateFormat: "MM/dd/yyyy" }).val()+"/"+h;
		var data = {
			name: $('#eventName').val(),
			type: $('#eventType').val(),
			playersLimit: $('#playersLimit').val(),
			dateEvent:   dateEvent,					
			city: $('#eventCity').val(),
			description: $('#eventDescription').val(),
			street: $('#eventAdress').val(),
			game: $('#gameName').val()
		};

		if (data.name != '' && data.playersLimit != '' && data.city != '') {
			
			$.ajax( { url: 'addEvent',
					  dataType: 'json',
					  data: data,
					  method: 'POST',
					  success: function(resp) {
						  if(resp.status == "added") {
						  	location.href="profil.html";
						  }
						  else if(resp.status == "databaseError") {
						  	alert("Błąd połączenia z bazą. Proszę spróbować ponownie.")
						  }
						  else if(resp.status == "dateFormatError") {
							alert("Wprowadź poprawne dane")
						  }
						  else {
						  	alert("Nie jesteś zalogowany. Zaloguj się, aby dodać wydarzenie.")
						  }
					  },
					  error: function() {
						alert("Wystapił błąd połączenia. Proszę spróbować później.");  
					  }
					});
		}
	
	});



});