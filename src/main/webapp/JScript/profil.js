var invalidEmail = true;
var invalidName = false;
var invalidSurname = false;
var invalidCity = false;
$(document).ready(function(){

	$("#nameField").css("display", "none");
	$("#surnameField").css("display", "none");
	$("#cityField").css("display", "none");
	$("#emailField").css("display", "none");
	$("#cancelButton").css("display", "none");
	$("#submitButton").css("display", "none");
	get();

	$('#cancelButton').on('click', function() {
		get();
		load();
	});
	$('#submitButton').on('click', function() {
		get();
		load();
		post();
	});
	$('#editButton').on('click', function() {
			
			$("#nameField").css("display", "initial");
			$("#surnameField").css("display", "initial");
			$("#cityField").css("display", "initial");
			$("#emailField").css("display", "initial");
			$("#cancelButton").css("display", "initial");
			
			$("#editButton").css("display", "none");
			
			document.getElementById("nameFieldInput").value =document.getElementById("name").innerHTML  ;
			document.getElementById("name").innerHTML = "";
			document.getElementById("surnameFieldInput").value =document.getElementById("surname").innerHTML  ;
			document.getElementById("surname").innerHTML = "";
			document.getElementById("cityFieldInput").value =document.getElementById("city").innerHTML  ;
			document.getElementById("city").innerHTML = "";
			document.getElementById("emailFieldInput").value =document.getElementById("email").innerHTML  ;
			document.getElementById("email").innerHTML = "";
			$('#emailFieldInput').trigger('start');
			$('#nameFieldInput').trigger('start');
			$('#surnameFieldInput').trigger('start');
			$('#cityFieldInput').trigger('start');
	});
		$('#emailFieldInput').on('change keyup start',function() {
		if(!validateEmail($(this).val()) || $(this).val().length >50) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidEmail = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidEmail = false;
			
		}
		submitVisibility();
	});

	$('#nameFieldInput').on('change keyup start',function() {
		if($(this).val().length >50) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidCity = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			//$(this).addClass("validRegistration");
			invalidCity = false;
		}
		submitVisibility();
	});
	
	$('#surnameFieldInput').on('change keyup start',function() {
		if($(this).val().length >50) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidSurname = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			//$(this).addClass("validRegistration");
			invalidSurname = false;
		}
		submitVisibility();
	});
	
	$('#cityFieldInput').on('change keyup start',function() {
		if($(this).val().length >100) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidCity = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			//$(this).addClass("validRegistration");
			invalidCity = false;
		}
		submitVisibility();
	});
}); 
submitVisibility = function(){
	if( !invalidEmail  && !invalidName && !invalidSurname && !invalidCity) {
			$("#submitButton").css("display", "initial");
		}
	else {
		$("#submitButton").css("display", "none");
	}
}
get = function(){
	$.ajax( { url: 'getUserData',
		  dataType: 'json',
		  data: {},
		  method: 'GET',
		  success: function(resp) {
			  
			  for (var i in resp) {
				  if (resp[i] == "null")
					  resp[i] = "";
			  }
			  
			  document.getElementById("name").innerHTML = resp.name;
			  document.getElementById("surname").innerHTML = resp.surname;
			  document.getElementById("city").innerHTML = resp.city;
			  document.getElementById("email").innerHTML = resp.email;
		  },
		  error: function() {
			alert("Błąd połączenia. Proszę spróbować ponownie");  
		  }
		});
};
post = function(){
	var data = {
			email: $('#emailFieldInput').val(),
			name: $('#nameFieldInput').val(),
			surname: $('#surnameFieldInput').val(),
			city: $('#cityFieldInput').val()
		};

		if (data.email != '') {
			
			$.ajax( { url: 'editAccount',
					  dataType: 'json',
					  data: data,
					  method: 'POST',
					  success: function(resp) {
						  if(resp.status == "registered") {
						  	location.href="profil.html";
						  }
						  else if(resp.status == "incorectData") {
							  alert("Błąd w systemie. Proszę spróbować ponownie");
						  }

					  },
					  error: function() {
						alert("Błąd połączenia. Proszę spróbować ponownie");  
					  }
					});
		}
	
}
load = function(){
	$("#cancelButton").css("display", "none");
	$("#submitButton").css("display", "none");
	$("#editButton").css("display", "initial");
	$("#nameField").css("display", "none");
	$("#surnameField").css("display", "none");
	$("#cityField").css("display", "none");
	$("#emailField").css("display", "none");
	$("#cancelButton").css("display", "none");
	
};
function validateEmail(email) {
	var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return email.match(pattern);
}