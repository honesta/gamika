var table;
var dataToSend;
$(document).ready(function(){
	
	clear();
	get();
	$('#gameTypeField').css("display", "none");
	$('#gameLanguageField').css("display", "none");
	
	$('#checkGameType').on('click', function() {
		if(document.getElementById('checkGameType').checked){
			$('#gameTypeField').css("display", "");			
		}
		else{
			$('#gameTypeField').css("display", "none");
			document.getElementById('gameType').value ="Dowolny";
		}	
	});
	$('#checkGameLanguage').on('click', function() {
		if(document.getElementById('checkGameLanguage').checked){
			$('#gameLanguageField').css("display", "");			
		}
		else{
			$('#gameLanguageField').css("display", "none");
			document.getElementById('gameLanguage').value ="";
		}	
	});

	$('#filtr').on('click', function() {
		clear();
		
		var typ = $('#gameType').val();
		if(typ =="Dowolny") typ="";
		dataToSend = {
				language: $('#gameLanguage').val(),
				type: typ		
			};
		
		get();
	});
});


clear = function(){
	 table="<tr><th>Nazwa gry</th><th>Wersja</th><th>Język</th><th>Typ</th><th>PEGI</th>";
	 dataToSend = {
			 	language: "",
				type: ""				
			};
}
get = function(){
	$.ajax( { url: 'findGame',
		  dataType: 'json',
		  data: dataToSend,
		  method: 'GET',
		  success: function(resp) {
			  
			  for(i=0;i<resp.count;i++){
				  PEGI = resp.game[i].PEGI
				  if(PEGI ==0)
					  PEGI="";
				  resp.game[i].url= "#"
				  table += "<tr><td>"+
				  	resp.game[i].name+ "</td><td>"+
				  	resp.game[i].version+ "</td><td>"+
				  	resp.game[i].language+ "</td><td>"+
				  	resp.game[i].type+ "</td><td>"+
				  	PEGI+ "</td><td>";
				    table = table.replace(/<td>null</gi, "<td></");
					
			  }
			  document.getElementById("dataArray").innerHTML = table;
		  },
		  error: function() {
			alert("Błąd połączenia. Proszę spróbować ponownie");  
		  }
		});
}