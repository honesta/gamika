var evetnID;
$(document).ready( function() {
	eventID = getParameterByName('eventID');
	get();
	
	var invalidEventName = true;
	var invalidPlayersLimit= true;
	var invalidDateEvent = false;
	var invalidCity = true;
	var invalidStreet = false;
	var invalidGameName = false;
	var invalidDescription = false;
	var invalidTime = false;
	
	
	$("#submitButton").css("display", "none");
	$(function() {
	    $( "#datepicker" ).datepicker($.datepicker.regional[ "pl" ]);
	    $("#timepicker").timepicker({ timeFormat: 'G:i' });
	  });
	function submitVisibility(){
		
		if(!invalidEventName  && !invalidPlayersLimit && !invalidDateEvent && !invalidCity && !invalidStreet && !invalidGameName && !invalidDescription) {
			$("#submitButton").css("display", "initial");
			}
		else {
			$("#submitButton").css("display", "none");
		}
	}

	$('#eventName').on('change keyup start',function() {
		if($(this).val().length < 4 || $(this).val().length > 30) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidEventName = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidEventName = false;
		}
		submitVisibility();
	});
	$('#timepicker').on('change keyup start',function() {
		
		if(!validateTime($(this).val())) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidTime = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidTime = false;
		}
		submitVisibility();
	}); 
	function validateTime(time) {
		var pattern = /^[0-9]{1,2}:[0-5]0/;
		return time.match(pattern);
	}
	$('#playersLimit').on('change keyup start',function() {
		var maxPlayers = parseInt($(this).val());
		if($(this).val().length < 1 || $(this).val().length > 5) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidPlayersLimit = true;
		}
		else if(isNaN(maxPlayers)) { 
			$(this).removeClass("validRegistration"); 
			$(this).addClass("invalidRegistration"); 
			invalidPlayersLimit = true; 
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidPlayersLimit = false;
		}
		submitVisibility();
	});
	
	$('#eventCity').on('change keyup start',function() {
		if($(this).val().length < 3 || $(this).val().length > 30) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidCity = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidCity = false;
		}
		submitVisibility();
	});
	
	$('#eventStreet').on('change keyup start',function() {
		if($(this).val().length > 30) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidStreet = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidStreet = false;
		}
		submitVisibility();
	});
	
	$('#gameName').on('change keyup start',function() {
		if($(this).val().length > 30) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidGameName = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidGameName = false;
		}
		submitVisibility();
	});
	
	$('#description').on('change keyup start',function() {
		if($(this).val().length > 150) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidDescription = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidDescription = false;
		}
		submitVisibility();
	});

	$('#cancelButton').on('click', function() {
		location.href="infoEvent.html?eventID="+getParameterByName('eventID');
	});
	$('#submitButton').on('click', function() {
		var h = $('#timepicker').val();
		var dateTypeVar = $('#datepicker').datepicker('getDate');
		$.datepicker.formatDate('dd-mm-yy', dateTypeVar);
		var dateEvent = $("#datepicker").datepicker({ dateFormat: "MM/dd/yyyy" }).val()+"/"+h;
		var data = {
			eventID : eventID,
			name: $('#eventName').val(),
			type: $('#eventType').val(),
			playersLimit: $('#playersLimit').val(),
			dateEvent:   dateEvent,					
			city: $('#eventCity').val(),
			description: $('#eventDescription').val(),
			street: $('#eventAdress').val(),
			game: $('#gameName').val()
		};

		if (data.name != '' && data.playersLimit != '' && data.city != '') {
			
			$.ajax( { url: 'updateEvent',
					  dataType: 'json',
					  data: data,
					  method: 'POST',
					  success: function(resp) {
						  if(resp.status == "updated") {
						  	location.href="infoEvent.html?eventID="+eventID;
						  }
						  else if(resp.status == "databaseError") {
						  	alert("Błąd połączenia z bazą. Proszę spróbować ponownie.")
						  }
						  else if(resp.status == "dateFormatError") {
							alert("Wprowadź poprawne dane")
						  }
						  else {
						  	alert("Nie jesteś zalogowany. Zaloguj się, aby dodać wydarzenie.")
						  }
					  },
					  error: function() {
						alert("Wystapił błąd połączenia. Proszę spróbować później.");  
					  }
					});
		}
	
	});
});
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function get()
{
	$.ajax( { url: 'infoEvent',
			dataType: 'json',
			data: {
				eventID :getParameterByName('eventID')
			},
			method: 'GET',
			success: function(resp) {
				if(resp.name != null && resp.name != "null")
					document.getElementById("eventName").value = resp.name;
				if(resp.game != null && resp.game != "null")
					document.getElementById("gameName").value = resp.game;
				if(resp.dateEvent != null && resp.dateEvent != "null"){
					var date = new Date(resp.dateEvent);
					var month = date.getMonth() + 1;
					var hour = date.getHours();
					var minute = date.getMinutes();
					if(month < 10)
						month = "0" + month;
					if(hour < 10)
						hour = "0" + hour;
					if(minute < 10)
						minute = "0" + minute;
					$('#datepicker').datepicker({ dateFormat: 'dd-mm-yyyy'}).datepicker("setDate", new Date(date.getDate() + "-" + month + "-" + date.getFullYear()));
					document.getElementById("timepicker").value = hour + ":" + minute;
				}
				if(resp.city != null && resp.city != "null")	
					document.getElementById("eventCity").value = resp.city;
				if(resp.type != null && resp.type != "null")
					document.getElementById("eventType").value = resp.type;
				if(resp.playersLimit != null && resp.playersLimit != "null")
					document.getElementById("playersLimit").value = resp.playersLimit;
				if(resp.street != null && resp.street != "null")
					document.getElementById("eventAdress").value = resp.street;
				if(resp.description != null && resp.description != "null")
					document.getElementById("eventDescription").value = resp.description;
				refresh();
			},
			error: function() {
				alert("Błąd połączenia. Proszę spróbować ponownie!");  
			}
		});
}
function refresh(){
	$('#eventName').trigger('start');
	$('#timepicker').trigger('start');
	$('#playersLimit').trigger('start');
	$('#eventCity').trigger('start');
	$('#eventStreet').trigger('start');
	$('#gameName').trigger('start');
	$('#description').trigger('start');
}