$(document).ready(function(){
	
	$.ajax( { url: 'infoEvent',
			dataType: 'json',
			data: {
				eventID :getParameterByName('eventID')
			},
			method: 'GET',
			success: function(resp) {
				if(resp.name != null && resp.name != "null")
					document.getElementById("eventName").innerHTML = resp.name;
				if(resp.game != null && resp.game != "null")
					document.getElementById("gameName").innerHTML = resp.game;
				if(resp.dateEvent != null && resp.dateEvent != "null"){
					var date = new Date(resp.dateEvent);
					var month = date.getMonth() + 1;
					var hour = date.getHours();
					var minute = date.getMinutes();
					if(month < 10)
						month = "0" + month;
					if(hour < 10)
						hour = "0" + hour;
					if(minute < 10)
						minute = "0" + minute;

					document.getElementById("dateEvent").innerHTML = date.getDate() + "-" + month + "-" + date.getFullYear();
					document.getElementById("hourEvent").innerHTML = hour + ":" + minute;
				}
					
					
				if(resp.city != null && resp.city != "null")
					document.getElementById("city").innerHTML = resp.city;
				if(resp.type != null && resp.type != "null")
					document.getElementById("type").innerHTML = resp.type;
				if(resp.playersLimit != null && resp.playersLimit != "null")
					document.getElementById("playersLimit").innerHTML = resp.playersLimit;
				if(resp.street != null && resp.street != "null")
					document.getElementById("street").innerHTML = resp.street;
				if(resp.description != null && resp.description != "null")
					document.getElementById("description").innerHTML = resp.description;
			},
			error: function() {
				alert("Błąd połączenia. Proszę spróbować ponownie!");  
			}
		});
	$('#editButton').on('click', function() {
		location.href="updateEvent.html?eventID="+getParameterByName('eventID');
	});
}) 
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}