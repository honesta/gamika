$(document).ready( function() {
	var invalidGameName = true;
	var invalidGameVersion= true;
	var invalidGameLanguage = true;
	var invalidGameRules = false;
	var invalidGameAuthor = false;
	var invalidGamePEGI = false;
	var invalidGamePlayersLimit = false;
	
	$("#submit").css("display", "none");
	
	function submitVisibility(){
		
		if(!invalidGameName  && !invalidGameVersion && !invalidGameLanguage && !invalidGameRules && !invalidGameAuthor
			&& !invalidGamePEGI && !invalidGamePlayersLimit) {
			$("#submit").css("display", "initial");
			}
		else {
			$("#submit").css("display", "none");
		}
	}

	$('#gameName').on('change keyup ',function() {
		if($(this).val().length < 4 || $(this).val().length > 30) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidGameName = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidGameName = false;
		}
		submitVisibility();
	});
	$('#gamePlayersLimit').on('change keyup ',function() {
		var maxPlayers = parseInt($(this).val());
		if( $(this).val().length > 2) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidGamePlayersLimit = true;
		}
		else if(isNaN(maxPlayers) && $(this).val().length>0) { 
			$(this).removeClass("validRegistration"); 
			$(this).addClass("invalidRegistration"); 
			invalidGamePlayersLimit = true; 
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidGamePlayersLimit = false;
		}
		submitVisibility();
	});
		$('#gameVersion').on('change keyup ',function() {
		var gameVersion = parseInt($(this).val());
		if($(this).val().length < 1 || $(this).val().length > 2 || $(this).val()=="0") {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidGameVersion = true;
		}
		else if(isNaN(gameVersion)) { 
			$(this).removeClass("validRegistration"); 
			$(this).addClass("invalidRegistration"); 
			invalidGameVersion = true; 
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidGameVersion = false;
		}
		submitVisibility();
	});
		$('#gamePEGI').on('change keyup ',function() {
		var gamePEGI = parseInt($(this).val());
		if($(this).val().length > 2) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidGamePEGI = true;
		}
		else if(isNaN(gamePEGI) && $(this).val().length>0) { 
			$(this).removeClass("validRegistration"); 
			$(this).addClass("invalidRegistration"); 
			invalidGamePEGI = true; 
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidGamePEGI = false;
		}
		submitVisibility();
	});
	$('#gameLanguage').on('change keyup ',function() {
		if($(this).val().length < 3 || $(this).val().length > 20) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidGameLanguage = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidGameLanguage = false;
		}
		submitVisibility();
	});
	
	$('#gameAuthor').on('change keyup ',function() {
		if($(this).val().length > 45) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidGameAuthor = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidGameAuthor = false;
		}
		submitVisibility();
	});
	
	$('#gameName').on('change keyup ',function() {
		if($(this).val().length > 30) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidGameName = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidGameName = false;
		}
		submitVisibility();
	});
	
	$('#gameRules').on('change keyup ',function() {
		if($(this).val().length > 500) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidGameRules = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidGameRules = false;
		}
		submitVisibility();
	});

	
	$('#submit').on('click', function() {
		var data = {
			name: $('#gameName').val(),
			version: $('#gameVersion').val(),
			language: $('#gameLanguage').val(),
			rules: $('#gameRules').val(),					
			author: $('#gameAuthor').val(),
			PEGI: $('#gamePEGI').val(),
			playersLimit: $('#gamePlayersLimit').val(),
			type: $('#gameType').val()
		};

		if (data.name != '' && data.version && data.language != '' ) {
			
			$.ajax( { url: 'addGame',
					  dataType: 'json',
					  data: data,
					  method: 'POST',
					  success: function(resp) {
						  if(resp.status == "added") {
						  	location.href="profil.html";
						  }
						  else if(resp.status == "databaseError") {
						  	alert("Błąd połączenia z bazą. Proszę spróbować ponownie.")
						  }
						  else if(resp.status == "dateFormatError") {
							alert("Wprowadź poprawne dane")
						  }
						  else {
						  	alert("Nie jesteś zalogowany. Zaloguj się, aby dodać wydarzenie.")
						  }
					  },
					  error: function() {
						alert("Wystapił błąd połączenia. Proszę spróbować później.");  
					  }
					});
		}
	
	});



});