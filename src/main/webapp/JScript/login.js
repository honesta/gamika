$(document).ready( function() {
	
	var invalidLogin = false;
	$("#SignIn").css("display", "none");
	
	function submitVisibility(){
		if(!invalidLogin) {
				$("#SignIn").css("display", "initial");
			}
		else {
			$("#SignIn").css("display", "none");
		}
	}
	
	$('#login').on('change keyup',function() {
		if($(this).val().length < 6 || $(this).val().length > 20) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			
			invalidLogin = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidLogin = false;
			
		}
		submitVisibility();
	});
	
	$('#SignIn').on('click', function() {
		var data = {
			
			password: $('#password').val(),
			login: $('#login').val(),
			
		};

		if ( data.password != '' && data.email != '') {
			
			$.ajax( { url: 'signIn',
					  dataType: 'json',
					  data: data,
					  method: 'POST',
					  success: function(resp) {
						  if(resp.status == "loggedIn") {
						  	location.href="profil.html";
						  }
						  else if(resp.status == "incorrectData") {
						  	alert("Podano niewłaściwy login lub hasło!");
						  }
						  else {
						  	alert("Błąd połączenia z bazą danych. Spróbuj ponownie");
						  }
					  },
					  error: function() {
						alert("Błąd połączenia. Proszę spróbować ponownie");  
					  }
					});
		}
	});
});