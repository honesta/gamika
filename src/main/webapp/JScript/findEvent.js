var table;
var dataToSend;
$(document).ready(function(){
	clear();
	get();
	$('#checkgameNameField').css("display", "none");
	$('#checkEventNameField').css("display", "none");
	$('#eventNameField').css("display", "none");
	$('#gameNameField').css("display", "none");
	$('#eventDateFromField').css("display", "none");
	$('#eventDateToField').css("display", "none");
	$('#eventTypeField').css("display", "none");
	$('#eventCityField').css("display", "none");
	$(function() {
	    $( "#datepickerFrom" ).datepicker($.datepicker.regional[ "pl" ]);
	    $( "#datepickerTo" ).datepicker($.datepicker.regional[ "pl" ]);
	});
	$('#checkEventName').on('click', function() {
		if(document.getElementById('checkEventName').checked){
			$('#eventNameField').css("display", "");			
		}
		else{
			$('#eventNameField').css("display", "none");
			document.getElementById('eventName').value ="";
		}	
	});
	$('#checkGameName').on('click', function() {
		if(document.getElementById('checkGameName').checked){
			$('#gameNameField').css("display", "");			
		}
		else{
			$('#gameNameField').css("display", "none");
			document.getElementById('gameName').value ="";
		}	
	});
	$('#checkEventDateFrom').on('click', function() {
		if(document.getElementById('checkEventDateFrom').checked){
			$('#eventDateFromField').css("display", "");
		} 
		else{
			$('#eventDateFromField').css("display", "none");
			
		}	
	});
	$('#checkEventDateTo').on('click', function() {
		if(document.getElementById('checkEventDateTo').checked){
			$('#eventDateToField').css("display", "");
		}
		else{
			$('#eventDateToField').css("display", "none");
			
		}	
	});
	$('#checkEventType').on('click', function() {
		if(document.getElementById('checkEventType').checked){
			$('#eventTypeField').css("display", "");			
		}
		else{
			$('#eventTypeField').css("display", "none");
			document.getElementById('eventType').value ="Dowolny";
		}	
	});
	$('#checkEventCity').on('click', function() {
		if(document.getElementById('checkEventCity').checked){
			$('#eventCityField').css("display", "");			
		}
		else{
			$('#eventCityField').css("display", "none");
			document.getElementById('eventCity').value ="";
		}	
	});
	$('#filtr').on('click', function() {
		clear();
		
		var dateTypeVar = $('#datepickerFrom').datepicker('getDate');
		$.datepicker.formatDate('dd-mm-yy', dateTypeVar);
		var dateEventFrom = $("#datepickerFrom").datepicker({ dateFormat: "MM/dd/yyyy" }).val();
		
		var dateTypeVar = $('#datepickerTo').datepicker('getDate');
		$.datepicker.formatDate('dd-mm-yy', dateTypeVar);
		var dateEventTo = $("#datepickerTo").datepicker({ dateFormat: "MM/dd/yyyy" }).val();
		if(!document.getElementById('checkEventDateFrom').checked){ 
			dateEventFrom ="";
		}
		if(!document.getElementById('checkEventDateTo').checked){ 
			dateEventTo ="";
		}
		var typ = $('#eventType').val();
		if(typ =="Dowolny") typ="";
		dataToSend = {
				name: $('#eventName').val(),
				type: typ,				
				dateEventFrom:   dateEventFrom,
				dateEventTo:   dateEventTo,
				city: $('#eventCity').val(),
				game: $('#gameName').val()
			};
		
		get();
	});
});


clear = function(){
	 table="<tr><th>Nazwa wydarzenia</th><th>Nazwa gry</th><th>Właściciel</th><th>Data</th><th>Godzina</th><th>Typ</th><th>Miasto</th><th></th></tr>";
	 dataToSend = {
				name: "",
				type: "",				
				dateEventFrom:   "",	
				dateEventTo:   "",
				city: "",
				game: ""
			};
}
changeJoin = function(ID)
{
	var button =document.getElementById('buttonJoin'+ID).innerHTML;
	if(	document.getElementById('buttonJoin'+ID).innerHTML=="Dołącz"){
		join(ID)
		}
	else{
		leave(ID)
	}
}
join = function(ID)
{
	joinData = {
			eventID : ID
		};
	$.ajax( { url: 'joinToEvent',
		  dataType: 'json',
		  data: joinData,
		  method: 'POST',
		  success: function(resp) {
			  if(resp.status == "joined") {
				  alert("Dołączono");
				  document.getElementById('buttonJoin'+ID).innerHTML = "Opuść";
				
			  }
			  else if(resp.status == "alredyExists") {
			  	alert("Już dołączyłeś do tego wydarzenia");
			  }
			  else {
			  	alert("Błąd w systemie. Proszę spróbować ponownie");
			  }
		  },
		  error: function() {
			alert("Błąd połączenia. Proszę spróbować ponownie");  
		  }
		});
}
leave = function(ID)
{
	leaveData = {
			eventID : ID
		};
	$.ajax( { url: 'leaveEvent',
		  dataType: 'json',
		  data: leaveData,
		  method: 'POST',
		  success: function(resp) {
			  if(resp.status == "deleted") {
				  alert("Usunięto");
				  document.getElementById('buttonJoin'+ID).innerHTML = "Dołącz";
			  }
			   else {
			  	alert("Błąd w systemie. Proszę spróbować ponownie");
			  }
		  },
		  error: function() {
			alert("Błąd połączenia. Proszę spróbować ponownie");  
		  }
		});
}
get = function(){
	$.ajax( { url: 'findEvent',
		  dataType: 'json',
		  data: dataToSend,
		  method: 'GET',  
		  success: function(resp) {
			  
			  for(i=0;i<resp.count;i++){
				  table += "<tr><td><a  href='infoEvent.html?eventID="+
				  	resp.event[i].eventID+ "'>" +
				  	resp.event[i].name+ "</a></td><td>"+
				  	resp.event[i].game+ "</td><td>"+
				  	resp.event[i].owner+ "</td><td>"+
				  	resp.event[i].dateEvent+ "</td><td>"+
				  	resp.event[i].hour+ "</td><td>"+
				  	resp.event[i].type+ "</td><td>"+
				  	resp.event[i].city+ "</td>";
				  	
				  	
				  		if(resp.event[i].joined !=1 ){
				  			table+=	"<td ><button id='buttonJoin"+resp.event[i].eventID+"' onclick='changeJoin("+resp.event[i].eventID+")' >Dołącz</button></td>";			  	
						  	
				  		}
				  		else{
				  						  	
						  	table+=	"<td><button id='buttonJoin"+resp.event[i].eventID+"' onclick='changeJoin("+resp.event[i].eventID+")'  >Opuść</button></td>";
				  		}
				  	table+=	"</tr>";
				  
			  }
			  table = table.replace(/<td>null</gi, "<td></");
			  document.getElementById("dataArray").innerHTML = table;
		  },
		  error: function() {
			alert("Błąd połączenia. Proszę spróbować ponownie");  
		  }
		});
	
}