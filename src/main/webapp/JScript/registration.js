$(document).ready( function() {
	var invalidLogin = true;
	var invalidPassword = true;
	var invalidPasswordSecond = true;
	var invalidEmail = true;
	var invalidName = false;
	var invalidSurname = false;
	var invalidCity = false;
	$("#register").css("display", "none");

	function submitVisibility(){
		if(!invalidLogin && !invalidPassword && !invalidEmail && !invalidPasswordSecond && !invalidName && !invalidSurname && !invalidCity) {
				$("#register").css("display", "initial");
			}
		else {
			$("#register").css("display", "none");
		}
	}
	
	$('#login').on('change keyup ',function() {
		if($(this).val().length < 6 || $(this).val().length > 20) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			
			invalidLogin = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidLogin = false;
			
		}
		submitVisibility();
	});

	$('#password').on('change keyup ',function() {
		if($(this).val().length < 6) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidPassword = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidPassword = false;
		}
		submitVisibility();
	});
	$('#passwordSecond').on('change keyup ',function() {
		if($(this).val() != $('#password').val()) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidPasswordSecond = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidPasswordSecond = false;
		}
		submitVisibility();
	});
	$('#email').on('change keyup ',function() {
		if(!validateEmail($(this).val()) || $(this).val().length >50) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidEmail = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			$(this).addClass("validRegistration");
			invalidEmail = false;
			
		}
		submitVisibility();
	});

	$('#name').on('change keyup ',function() {
		if($(this).val().length >50) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidCity = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			//$(this).addClass("validRegistration");
			invalidCity = false;
		}
		submitVisibility();
	});
	
	$('#surname').on('change keyup ',function() {
		if($(this).val().length >50) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidSurname = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			//$(this).addClass("validRegistration");
			invalidSurname = false;
		}
		submitVisibility();
	});
	
	$('#city').on('change keyup ',function() {
		if($(this).val().length >100) {
			$(this).removeClass("validRegistration");
			$(this).addClass("invalidRegistration");
			invalidCity = true;
		}
		else {
			$(this).removeClass("invalidRegistration");
			//$(this).addClass("validRegistration");
			invalidCity = false;
		}
		submitVisibility();
	});
	$('#register').on('click', function() {
	
		var data = {
			login: $('#login').val(),
			password: $('#password').val(),
			email: $('#email').val(),
			name: $('#name').val(),
			surname: $('#surname').val(),
			city: $('#city').val()
		};

		if (data.login != '' && data.password != '' && data.email != '') {
			
			$.ajax( { url: 'createAccount',
					  dataType: 'json',
					  data: data,
					  method: 'POST',
					  success: function(resp) {
						  if(resp.status == "registered") {
						  	location.href="index.html";
						  }
						  else if(resp.status == "alredyExists") {
						  	alert("Podany użytkownik został już zarejestrowany");
						  }
						  else {
						  	alert("Błąd w systemie. Proszę spróbować ponownie");
						  }
					  },
					  error: function() {
						alert("Błąd połączenia. Proszę spróbować ponownie");  
					  }
					});
		}
	
	});

	function validateEmail(email) {
		var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return email.match(pattern);
	}

});