package databaseData;

// TODO: Auto-generated Javadoc
/**
 * The Class UserEvent.
 */
public class UserEvent {

	/** The login.  of user*/
	private String login;
	
	/** The id  of event*/
	private int eventID;

	/**
	 * Instantiates a new user event.
	 *
	 * @param login the login
	 * @param eventID the event id
	 */
	public UserEvent(String login, int eventID) {
		super();
		this.login = login;
		this.eventID = eventID;
	}

	/**
	 * Gets the login.
	 *
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Gets the event id.
	 *
	 * @return the event id
	 */
	public int getEventID() {
		return eventID;
	}
}
