package databaseData;

public class GameFilter {

	/** The language. */
	private String language ;

	/** The type. */
	private String type = null;
	


	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
