package databaseData;

import gamika.security.Password;

/**
 * The Class User to store data from and to database.
 */
public class User {
	
	/** The login.  of user*/
	private final String login;
	
	/** The password with salt. */
	private final Password password;
	
	/** The email of user. */
	private final String email;
	
	/** The name of user. */
	private String name;
	
	/** The surname of user. */
	private String surname;
	
	/** The city. */
	private String city;
	
	/** The photo. */
	private byte[] photo;
	
	/**
	 * Instantiates a new user.
	 *
	 * @param login the login
	 * @param password the password
	 * @param email the email
	 */
	public User(String login, Password password, String email) {
		this.login = login;
		this.password = password;
		this.email = email;
	}
	
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the surname.
	 *
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}
	
	/**
	 * Sets the surname.
	 *
	 * @param surname the new surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	
	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	/**
	 * Gets the photo.
	 *
	 * @return the photo
	 */
	public byte[] getPhoto() {
		return photo;
	}
	
	/**
	 * Sets the photo.
	 *
	 * @param photo the new photo
	 */
	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}
	
	/**
	 * Gets the login.
	 *
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public byte[] getPassword() {
		return password.getHash();
	}

	/**
	 * Gets the salt.
	 *
	 * @return the salt
	 */
	public byte[] getSalt() {
		return password.getSalt();
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	
	
}
