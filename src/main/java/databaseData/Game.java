package databaseData;

public class Game {
	
	/** The name. */
	private String name;
	
	/** The rules. */
	private String rules;
	
	/** The  EGI number. */
	private int PEGI ;
	
	/** The version. */
	private int version ;
	
	/** The author. */
	private String author;
	
	/** The language. */
	private String language;
	
	/** The players limit. */
	private int playersLimit ;
	
	/** The type. */
	private String type;
	/**
	 * @return the rules
	 */
	public String getRules() {
		return rules;
	}
	/**
	 * @param rules the rules to set
	 */
	public void setRules(String rules) {
		this.rules = rules;
	}
	/**
	 * @return the pEGI
	 */
	public int getPEGI() {
		return PEGI;
	}
	/**
	 * @param PEGI the PEGI to set
	 */
	public void setPEGI(int PEGI) {
		this.PEGI = PEGI;
	}
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @return the playersLimit
	 */
	public int getPlayersLimit() {
		return playersLimit;
	}
	/**
	 * @param playersLimit the players limit to set
	 */
	public void setPlayersLimit(int playersLimit) {
		this.playersLimit = playersLimit;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the version
	 */
	public int getVersion() {
		return version;
	}
	public Game(String name, int version, String language) {
		super();
		this.name = name;
		this.version = version;
		this.PEGI = 0;
		this.playersLimit = 0;
		this.language = language;
	}
}
