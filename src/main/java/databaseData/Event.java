package databaseData;

import java.util.Date;


/**
 * The Class Event.
 */
public class Event {
	
	/** The id. */
	private int ID;
	
	/** The name. */
	private final String name;
	
	/** The type. */
	private final String type;
	
	/** The players limit. */
	private final int playersLimit;
	
	/** The date event. */
	private final Date dateEvent;
	
	/** The city. */
	private final String city;
	
	/** The owner login. */
	private final String ownerLogin;
	
	/** The owner login. */
	private int joined;


	/** The street. */
	private String street;
	
	/** The game. */
	private String game;
	
	/** The description. */
	private String description;
	
	/**
	 * Instantiates a new event.
	 *
	 * @param name the name
	 * @param type the type
	 * @param playersLimit the players limit
	 * @param dateEvent the date event
	 * @param city the city
	 * @param ownerLogin the owner login
	 */
	public Event(String name, String type, int playersLimit, Date dateEvent, String city, String ownerLogin) {
		super();
		this.name = name;
		this.type = type;
		this.playersLimit = playersLimit;
		this.dateEvent = dateEvent;
		this.city = city;
		this.ownerLogin = ownerLogin;
	}
	
	/**
	 * Gets the street.
	 *
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}
	
	/**
	 * Sets the street.
	 *
	 * @param street the new street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
	/**
	 * Gets the game.
	 *
	 * @return the game
	 */
	public String getGame() {
		return game;
	}
	
	/**
	 * Sets the game.
	 *
	 * @param game the new game
	 */
	public void setGame(String game) {
		this.game = game;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getID() {
		return ID;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Gets the players limit.
	 *
	 * @return the players limit
	 */
	public int getPlayersLimit() {
		return playersLimit;
	}
	
	/**
	 * Gets the date event.
	 *
	 * @return the date event
	 */
	public Date getDateEvent() {
		return dateEvent;
	}
	
	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	
	/**
	 * Gets the owner login.
	 *
	 * @return the owner login
	 */
	public String getOwnerLogin() {
		return ownerLogin;
	}
	public void setID(int iD) {
		ID = iD;
	}

	public int getJoined() {
		return joined;
	}
	public String getJoinedString() {
		return Integer.toString(joined);
	}

	public void setJoined(int joined) {
		this.joined = joined;
	}
	
}
