package databaseEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import databaseData.User;
import databaseData.UserEvent;


/**
 * The Class represent userevent entity.
 */
public class UsersEvents {
	/** The name of relation. */
	private static String RELATION = "gamika.usersevents";

	/** The instance. */
	private static UsersEvents instance;
	
	/** The stmt. */
	private java.sql.Statement stmt = null;

	/** The rs. */
	private ResultSet rs = null;
	
	/** The sql query. */
	private String sqlQuery= "";
	
	/**
	 * Gets the single instance of Users.
	 *
	 * @return single instance of Users
	 */
	public static UsersEvents getInstance() {
		if (instance == null)
			instance = new UsersEvents();
		return instance;
	}
	
	/**
	 * Sets the instance(use in test).
	 *
	 * @param newInstance the new instance
	 */
	static void setInstance(UsersEvents newInstance) {
		instance = newInstance;
	}
	
	/**
	 * Adds the user to datatable.
	 *
	 * @param userEvent the user event
	 * @throws UserEventAlreadyExistsException the user event already exists exception
	 * @throws DatabaseException             the database exception
	 */
	public void addUserEvent(UserEvent userEvent) throws UserEventAlreadyExistsException, DatabaseException {
		if (userEvent != null) {
			StringBuffer columns = new StringBuffer(UsersEventsEnum.LOGIN.toString() + "," + UsersEventsEnum.EVENTID.toString());
			PreparedStatement preparedStatement;
			try {
				Database database = Database.getInstance();
				sqlQuery = "INSERT INTO " + RELATION + "(" + columns
						+ ") VALUES (?,?)";
				preparedStatement = database.getConnection().prepareStatement(sqlQuery);
				preparedStatement.setString(1, userEvent.getLogin());
				preparedStatement.setInt(2, userEvent.getEventID());
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				if (e.getErrorCode() == 1062)
					throw new UserEventAlreadyExistsException(e);
				else
					throw new DatabaseException(e);
			}
		}
	}
	
	/**
	 * Delete user event.
	 *
	 * @param userEvent the user event
	 * @throws SQLException the SQL exception
	 */
	public void deleteUserEvent(UserEvent userEvent) throws SQLException    {
		if (userEvent != null) {
			
			StringBuffer columns = new StringBuffer(UsersEventsEnum.LOGIN.toString() + " = ? AND " + UsersEventsEnum.EVENTID.toString()+ " = ?");
			PreparedStatement preparedStatement;
			
				Database database = Database.getInstance();
				sqlQuery = "DELETE FROM "+RELATION +" WHERE "+ columns; 
				
				preparedStatement = database.getConnection().prepareStatement(sqlQuery);
				preparedStatement.setString(1, userEvent.getLogin());
				preparedStatement.setInt(2, userEvent.getEventID());
				preparedStatement.executeUpdate();
			
		}
	}
}
