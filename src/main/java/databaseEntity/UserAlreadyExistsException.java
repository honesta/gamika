package databaseEntity;

/**
 * The Class UserAlreadyExistsException.
 */
public class UserAlreadyExistsException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3645393932277558409L;

	/**
	 * Constructs with the given throwable.
	 *
	 * @param t the throwable to throw
	 */
	public UserAlreadyExistsException(Throwable t) {
		super(t);
	}

	/**
	 * Constructs with the given message.
	 *
	 * @param message the message of the exception
	 */
	public UserAlreadyExistsException(String message) {
		super(message);
	}

	/**
	 * Constructs with the given message and the original throwable cause.
	 *
	 * @param message the message of the exception
	 * @param t the original throwable
	 */
	public UserAlreadyExistsException(String message, Throwable t) {
		super(message, t);
	}
}