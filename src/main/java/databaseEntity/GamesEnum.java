/**
 * 
 */
package databaseEntity;


public enum GamesEnum {
	
	/** The name of game. */
	NAME{
		public String toString() {
			return "name";
		}
	},
	
	/** The rules of game. */
	RULES{
		public String toString() {
			return "rules";
		}
	},
	
	/** The PEGI of game. */
	PEGI{
		public String toString() {
			return "PEGI";
		}
	},
	
	/** The version of game. */
	VERSION{
		public String toString() {
			return "version";
		}
	},
	
	/** The author of game. */
	AUTHOR{
		public String toString() {
			return "author";
		}
	},
	
	/** The language of game. */
	LANGUAGE{
		public String toString() {
			return "language";
		}
	},
	
	/** The type of game. */
	TYPE{
		public String toString() {
			return "type";
		}
	},
	
	/** The player limit of game. */
	LIMIT {

		public String toString() {
			return "players_Limit";
		}
	},
}
