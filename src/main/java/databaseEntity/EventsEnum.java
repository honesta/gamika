package databaseEntity;


/**
 * ENUM to store name columns of entity.
 */
public enum EventsEnum {
	
	/** The id of event. */
	ID {

		public String toString() {
			return "ID";
		}

	},

	/** The name of event. */
	NAME {

		public String toString() {
			return "name";
		}

	},

	/** The type of event. */
	TYPE {

		public String toString() {
			return "type";
		}

	},

	/** The limit of players. */
	LIMIT {

		public String toString() {
			return "players_limit";
		}
	},
	
	/** The date of event. */
	DATE {

		public String toString() {
			return "date_event";
		}
	},
	
	/** The city of event. */
	CITY {

		public String toString() {
			return "city";
		}
	},
	
	/** The street of event. */
	STREET {

		public String toString() {
			return "street";
		}
	},
	
	/** The game which choosen for players. */
	GAME {

		public String toString() {
			return "game";
		}
	},
	
	/** The description of event. */
	DESCRIPTION {

		public String toString() {
			return "description";
		}
	},
	
	/** The person who create event. */
	ADMIN {

		public String toString() {
			return "user_login";
		}
	},
	/** The person who create event. */
	JOINED {

		public String toString() {
			return "event_id";
		}
	}
	
	
	
	
}
