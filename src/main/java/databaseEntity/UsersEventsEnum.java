package databaseEntity;

/**
 * The Enum UsersEventsEnum.
 */
public enum UsersEventsEnum {

	/** The login of user. */
	LOGIN {

		public String toString() {
			return "user_login";
		}
	},
	/** The id of event. */
	EVENTID {

		public String toString() {
			return "event_id";
		}

	}
}
