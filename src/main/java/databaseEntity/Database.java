package databaseEntity;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Class Database.
 *
 * Class to keep properties of connection with database
 */
public class Database {

	/** The instance of singleton. */
	private static Database instance = null;
	
	/**  The connection. */
	private static Connection conn = null;

	/**
	 * Gets the single instance of Database.
	 *
	 * @return single instance of Database
	 */
	public static Database getInstance() {
		if (instance == null) {
			instance = new Database();
		}
		return instance;
	}

	/**
	 * Constructor to do connection with database.
	 * Use password and login to choosen database.
	 */
	Database() {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost/gamika?user=webapp&password=0pl9ok8i");
			System.out.println("Połączono z bazą danych");
		} catch (SQLException ex) {
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, "Nie udało się połączyć z bazą danych", ex);
			System.exit(-1);
		} catch (InstantiationException e) {
		
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
		
			e.printStackTrace();
		}
	}

	/**
	 * Sets the instance(use in test).
	 *
	 * @param instance the new instance
	 */
	static void setInstance(Database instance) {
		Database.instance = instance;
	}
		
	/**
	 * Gets the connection with database.
	 *
	 * @return the connection
	 */
	public Connection getConnection() {
		return conn;
	}

}
