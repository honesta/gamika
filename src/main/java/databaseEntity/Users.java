package databaseEntity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;

import databaseData.User;
import gamika.security.Password;


/**
 * The Class Users to represent entity in database.
 */
public class Users {

	/** The name of relation. */
	private static String RELATION = "gamika.users";

	/** The instance. */
	private static Users instance;
	
	/** The stmt. */
	private java.sql.Statement stmt = null;

	/** The rs. */
	private ResultSet rs = null;
	
	/** The sql query. */
	private String sqlQuery= "";
	
	/**
	 * Gets the single instance of Users.
	 *
	 * @return single instance of Users
	 */
	public static Users getInstance() {
		if (instance == null)
			instance = new Users();
		return instance;
	}
	
	/**
	 * Sets the instance(use in test).
	 *
	 * @param newInstance the new instance
	 */
	static void setInstance(Users newInstance) {
		instance = newInstance;
	}
	
	/**
	 * Adds the user to datatable.
	 *
	 * @param user
	 *            the user
	 * @throws UserAlreadyExistsException
	 *             the user already exists exception
	 * @throws DatabaseException
	 *             the database exception
	 */
	public void addUser(User user) throws UserAlreadyExistsException, DatabaseException {
		if (user != null) {
			StringBuffer columns = new StringBuffer(UsersEnum.LOGIN.toString() + "," + UsersEnum.PASSWORD.toString()
					+ "," + UsersEnum.SALT.toString() + "," + UsersEnum.EMAIL.toString());
			StringBuffer valuesAfterSalt = new StringBuffer("");
			if (user.getName() != null) {
				columns.append("," + UsersEnum.NAME.toString());
				valuesAfterSalt.append("," + "?");
			}
			if (user.getSurname() != null) {
				columns.append("," + UsersEnum.SURNAME.toString());
				valuesAfterSalt.append("," + "?");
			}
			if (user.getCity() != null) {
				columns.append("," + UsersEnum.CITY.toString());
				valuesAfterSalt.append("," + "?");
			}

			PreparedStatement preparedStatement;
			try {
				Database database = Database.getInstance();
				sqlQuery = "INSERT INTO " + RELATION + "(" + columns
						+ ") VALUES (?,?,?,?" + valuesAfterSalt + ")";
				preparedStatement = database.getConnection().prepareStatement(sqlQuery);
				preparedStatement.setString(1, user.getLogin());
				preparedStatement.setBytes(2, user.getPassword());
				preparedStatement.setBytes(3, user.getSalt());
				preparedStatement.setString(4, user.getEmail());
				int count = 0;
				if (user.getName() != null) {
					count++;
					preparedStatement.setString(5, user.getName());
				}
				if (user.getSurname() != null) {
					count++;
					if(count == 1)
						preparedStatement.setString(5, user.getSurname());
					else 
						preparedStatement.setString(6, user.getSurname());
				}
				if (user.getCity() != null) {
					count++;
					if(count == 1)
						preparedStatement.setString(5, user.getCity());
					else if(count == 2)
						preparedStatement.setString(6, user.getCity());
					else
						preparedStatement.setString(7, user.getCity());
				}
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				if (e.getErrorCode() == 1062)
					throw new UserAlreadyExistsException(e);
				else
					throw new DatabaseException(e);
			}

		}
	}

	/**
	 * Gets the sql query.
	 *
	 * @return the sql query
	 */
	public String getSqlQuery() {
		return sqlQuery;
	}

	/**
	 * Gets the password with salt of users relation.
	 *
	 * @param user
	 *            the user
	 * @return the password
	 * @throws SQLException
	 *             the SQL exception
	 */
	public Password getPassword(String user) throws SQLException {
		Password password = null;
		Database database = Database.getInstance();
		if (stmt == null)
			stmt = database.getConnection().createStatement();
		sqlQuery = "SELECT password, salt FROM " + RELATION + " WHERE login='" + user + "'";
		rs = stmt.executeQuery(sqlQuery);
		if (rs.next()) {
			password = new Password(rs.getBytes("password"), rs.getBytes("salt"));
		}

		return password;
	}

	/**
	 * Gets the info about user to profil view.
	 *
	 * @param user the user
	 * @return the info about user
	 * @throws SQLException the SQL exception
	 */
	public User getInfoAboutUser(String user) throws SQLException {
		Database database = Database.getInstance();
		User userInfo = null;
		if (stmt == null)
			stmt = database.getConnection().createStatement();
		sqlQuery = "SELECT email, name, surname, city FROM " + RELATION + " WHERE login='" + user + "'";
		rs = stmt.executeQuery(sqlQuery);
		if (rs.next()) {
			String email = rs.getString("email");
			String name = rs.getString("name");
			String surname = rs.getString("surname");
			String city = rs.getString("city");
			userInfo = new User(user, null, email);
			if(name != null && name != "" && name != "NULL") {
				userInfo.setName(name);
			}
			if(surname != null && surname != "" && surname != "NULL") {
				userInfo.setSurname(surname);
			}
			if(city != null && city != "" && city != "NULL") {
				userInfo.setCity(city);
			}
		}
		return userInfo;
	}
	
	/**
	 * Update user.
	 *
	 * @param user the user
	 * @throws SQLException the SQL exception
	 */
	public void updateUser(User user) throws SQLException{
		if (user != null) {
			StringBuffer query = new StringBuffer("UPDATE " + RELATION + " SET ");
			query.append(UsersEnum.CITY.toString() + "=?, ");
			query.append(UsersEnum.EMAIL.toString() + "=?, ");
			query.append(UsersEnum.NAME.toString() + "=?, ");
			query.append(UsersEnum.SURNAME.toString() + "=? WHERE ");
			query.append(UsersEnum.LOGIN.toString() + "=?");

			PreparedStatement preparedStatement;
			Database database = Database.getInstance();
			preparedStatement = database.getConnection().prepareStatement(query.toString());
			preparedStatement.setString(1, user.getCity());
			preparedStatement.setString(2, user.getEmail());
			preparedStatement.setString(3, user.getName());
			preparedStatement.setString(4, user.getSurname());
			preparedStatement.setString(5, user.getLogin());
			preparedStatement.executeUpdate();

		}
	}
	
	

}