package databaseEntity;

public class UserEventAlreadyExistsException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3645393932277558409L;
	
	public UserEventAlreadyExistsException() {
		
	}

	public UserEventAlreadyExistsException(String arg0) {
		super(arg0);
		
	}

	public UserEventAlreadyExistsException(Throwable arg0) {
		super(arg0);
		
	}

	public UserEventAlreadyExistsException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	public UserEventAlreadyExistsException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		
	}

}
