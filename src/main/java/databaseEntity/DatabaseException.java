package databaseEntity;

/**
 * The Class DatabaseException.
 */
public class DatabaseException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5667755550518706526L;

	/**
	 * Constructs with the given throwable.
	 *
	 * @param t the throwable to throw
	 */
	public DatabaseException(Throwable t) {
		super(t);
	}

	/**
	 * Constructs with the given message.
	 *
	 * @param message the message of the exception
	 */
	public DatabaseException(String message) {
		super(message);
	}

	/**
	 * Constructs with the given message and the original throwable cause.
	 *
	 * @param message the message of the exception
	 * @param t the original throwable
	 */
	public DatabaseException(String message, Throwable t) {
		super(message, t);
	}
}