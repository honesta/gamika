/**
 * 
 */
package databaseEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import databaseData.Event;
import databaseData.Game;
import databaseData.GameFilter;


public class Games {
	/** The relation. */
	private static String RELATION = "gamika.games";
	
	private static Games instance;

	/** The stmt. */
	private java.sql.Statement stmt = null;

	/** The rs. */
	private ResultSet rs = null;
	
	private String sqlQuery = "";

	/**
	 * Instantiates a new events.
	 */
	public Games() {
		try {
			Database database = Database.getInstance();
			stmt = database.getConnection().createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	public static Games getInstance() {
		if(instance == null)
			instance = new Games();
		return instance;
	}
	
	static void setInstance(Games newInstance) {
		instance = newInstance;
	}
	
	public String getSqlQuery() {
		return sqlQuery;
	}
	public void addGame(Game game) throws SQLException {
		if (game != null) {
			
			StringBuffer columns = new StringBuffer(GamesEnum.NAME.toString() + ","
					+ GamesEnum.VERSION.toString() + "," + GamesEnum.LANGUAGE.toString());

			StringBuffer values = new StringBuffer("'" + game.getName() + "','" + game.getVersion()+"','" + game.getLanguage() +"'");
						
			if (game.getAuthor() != null) {
				columns.append("," + GamesEnum.AUTHOR.toString());
				values.append("," + "'" + game.getAuthor() + "'");
			}
			if (game.getPEGI() != 0) {
				columns.append("," + GamesEnum.PEGI.toString());
				values.append("," + "'" + game.getPEGI() + "'");
			}
			if (game.getPlayersLimit() != 0) {
				columns.append("," + GamesEnum.LIMIT.toString());
				values.append("," + "'" + game.getPlayersLimit() + "'");
			}
			if (game.getRules() != null) {
				columns.append("," + GamesEnum.RULES.toString());
				values.append("," + "'" + game.getRules() + "'");
			}
			if (game.getType() != null) {
				columns.append("," + GamesEnum.TYPE.toString());
				values.append("," + "'" + game.getType() + "'");
			}
			PreparedStatement preparedStatement;
			Database database = Database.getInstance();
			sqlQuery = "INSERT INTO " + RELATION + "(" + columns + ") VALUES (" + values + ")";
			
			preparedStatement = database.getConnection()
					.prepareStatement(sqlQuery);
			preparedStatement.executeUpdate();

		}
	}
	public ArrayList<Game> getGames(GameFilter game) throws SQLException, ParseException {
		ArrayList<Game> games = new ArrayList<Game>();
		Database database = Database.getInstance();
		StringBuffer query = new StringBuffer("");
		String type = game.getType();
		String language = game.getLanguage();
		if(isSet(type)) {
			query.append(GamesEnum.TYPE.toString() + " LIKE '%" + type + "%' AND ");
		}
		if(isSet(language)){
			query.append(GamesEnum.LANGUAGE.toString() + " LIKE '%" + language + "%' AND ");
		}
		
		
		PreparedStatement preparedStatement;
		if(database == null) {
			database = Database.getInstance();
		} 
		String selectSQL = "SELECT * FROM " + RELATION;
		if(query.toString() != "" && query.toString().length()>0) {
			selectSQL+=" WHERE ";
			query.append("TRUE");
			selectSQL+= query;
		}
		
		System.out.println(selectSQL);
		preparedStatement = database.getConnection()
				.prepareStatement(selectSQL);
		
		rs = preparedStatement.executeQuery();
		String name;
		int version, PEGI;
		if(rs != null){
			while (rs.next()) {
				name = rs.getString(GamesEnum.NAME.toString());
				type = rs.getString(GamesEnum.TYPE.toString());
				version = rs.getInt(GamesEnum.VERSION.toString());
				language = rs.getString(GamesEnum.LANGUAGE.toString());
				PEGI = rs.getInt(GamesEnum.PEGI.toString());
				
				Game dbGame = new Game(name, version, language);
				
				if(isSet(type) && !type.equals("NULL")) {
					dbGame.setType(type);
				}
				if(PEGI!=0) {
					dbGame.setPEGI(PEGI);
				}
				games.add(dbGame);
			}
		}
		return games;
	}

	private boolean isSet(String type) {
		return type != null && type != "" && type.length()>0;
	}
}
