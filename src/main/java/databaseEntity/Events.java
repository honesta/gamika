package databaseEntity;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

import databaseData.Event;
import databaseData.EventFilter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * The Class Events.
 */
public class Events {

	/** The relation. */
	private static String RELATION = "gamika.events";
	
	private static Events instance;

	/** The stmt. */
	private java.sql.Statement stmt = null;

	/** The rs. */
	private ResultSet rs = null;
	
	private String sqlQuery = "";

	/**
	 * Instantiates a new events.
	 */
	public Events() {
		try {
			Database database = Database.getInstance();
			stmt = database.getConnection().createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * Gets the single instance of Events.
	 *
	 * @return single instance of Events
	 */
	public static Events getInstance() {
		if(instance == null)
			instance = new Events();
		return instance;
	}
	
	/**
	 * Sets the instance(use in test).
	 *
	 * @param newInstance the new instance
	 */
	static void setInstance(Events newInstance) {
		instance = newInstance;
	}
	
	/**
	 * Gets the sql query.
	 *
	 * @return the sql query
	 */
	public String getSqlQuery() {
		return sqlQuery;
	}

	/**
	 * Adds the event.
	 *
	 * @param event the event
	 * @throws SQLException the SQL exception
	 */
	public void addEvent(Event event) throws SQLException {
		if (event != null) {
			
			StringBuffer columns = new StringBuffer(EventsEnum.NAME.toString() + ","
					+ EventsEnum.TYPE.toString() + "," + EventsEnum.LIMIT.toString() + "," + EventsEnum.DATE.toString()
					+ "," + EventsEnum.ADMIN.toString() + "," + EventsEnum.CITY.toString());

			StringBuffer values = new StringBuffer("'" + event.getName() + "','" + event.getType()
					+ "'," + event.getPlayersLimit() + ", ?,'"
					+ event.getOwnerLogin() + "','" + event.getCity() + "'");
						
			if (event.getStreet() != null) {
				columns.append("," + EventsEnum.STREET.toString());
				values.append("," + "'" + event.getStreet() + "'");
			}
			if (event.getGame() != null) {
				columns.append("," + EventsEnum.GAME.toString());
				values.append("," + "'" + event.getGame() + "'");
			}
			if (event.getDescription() != null) {
				columns.append("," + EventsEnum.DESCRIPTION.toString());
				values.append("," + "'" + event.getDescription() + "'");
			}

			PreparedStatement preparedStatement;
			Database database = Database.getInstance();
			sqlQuery = "INSERT INTO " + RELATION + "(" + columns + ") VALUES (" + values + ")";
			preparedStatement = database.getConnection()
					.prepareStatement(sqlQuery);
			preparedStatement.setTimestamp(1, new java.sql.Timestamp(event.getDateEvent().getTime()));
			preparedStatement.executeUpdate();

		}
	}
	
	/**
	 * Gets the events to filter view.
	 *
	 * @param event the event
	 * @return the events
	 * @throws SQLException the SQL exception
	 * @throws ParseException the parse exception
	 */
	public  ArrayList<Event> getEvents(EventFilter event) throws SQLException, ParseException {
		ArrayList<Event> events = new ArrayList<Event>();
		Database database = Database.getInstance();
		StringBuffer query = new StringBuffer("");
		String type = event.getType();
		Date dateFrom = event.getDateFrom();
		Date dateTo = event.getDateTo();
		String city = event.getCity();
		if(isSet(type)) {
			query.append(EventsEnum.TYPE.toString() + " LIKE '%" + type + "%' AND ");
		}
		if( city != null && city != "") {
			query.append(EventsEnum.CITY.toString() + " LIKE '%" + city + "%' AND ");
		}
		
		
		if( isSet(dateFrom) && isSet(dateTo)) {
			query.append(EventsEnum.DATE.toString() + " BETWEEN ? AND ? AND ");
		}
		else if(isSet(dateFrom)) {
			query.append(EventsEnum.DATE.toString() + " > ? AND ");
		}
		else if(isSet(dateTo)) {
			query.append(EventsEnum.DATE.toString() + " < ? AND ");
		}
		if(query.toString() != "") {
			query.append("TRUE");
		}
		PreparedStatement preparedStatement;
		if(database == null) {
			database = Database.getInstance();
		} 
		String selectSQL = "SELECT * FROM "+ RELATION +" left join (select * from gamika.usersevents where usersevents.user_login like '"+ event.getLogin()+"') usersevents on gamika.events.id = usersevents.event_id"
				+ " WHERE " + query;
				
		//String selectSQL = "SELECT * FROM " + RELATION + " WHERE " + query;
		System.out.println(selectSQL);
		preparedStatement = database.getConnection()
				.prepareStatement(selectSQL);
		if( isSet(dateFrom) && isSet(dateTo)) {
			preparedStatement.setTimestamp(1, new java.sql.Timestamp(dateFrom.getTime()));
			preparedStatement.setTimestamp(2, new java.sql.Timestamp(dateTo.getTime()));
			
		}
		else if(isSet(dateFrom)) {
			preparedStatement.setTimestamp(1, new java.sql.Timestamp(dateFrom.getTime()));
		}
		else if(isSet(dateTo)) {
			preparedStatement.setTimestamp(1, new java.sql.Timestamp(dateTo.getTime()));
		}
		rs = preparedStatement.executeQuery();
		String name, street, game, description, user_login;
		int playersLimit,joined, id;
		if(rs != null){
			while (rs.next()) {
				name = rs.getString(EventsEnum.NAME.toString());
				type = rs.getString(EventsEnum.TYPE.toString());
				playersLimit = rs.getInt(EventsEnum.LIMIT.toString());
				city = rs.getString(EventsEnum.CITY.toString());
				street = rs.getString(EventsEnum.STREET.toString());
				game = rs.getString(EventsEnum.GAME.toString());
				description = rs.getString(EventsEnum.DESCRIPTION.toString());
				user_login = rs.getString(EventsEnum.ADMIN.toString());
				dateFrom = rs.getDate(EventsEnum.DATE.toString());
				id = rs.getInt(EventsEnum.ID.toString());
				if (rs.getObject(EventsEnum.JOINED.toString()) != null && !rs.wasNull()) {
					joined = rs.getInt(EventsEnum.JOINED.toString());
				}
				else
					joined =-1;
				
				Event dbEvent = new Event(name, type, playersLimit, dateFrom, city, user_login);
				dbEvent.setID(id);
				if(isSet(street) && !street.equals("NULL")) {
					dbEvent.setStreet(street);
				}
				if(isSet(game) && !game.equals("NULL")) {
					dbEvent.setGame(game);
				}
				if(isSet(description) && !description.equals("NULL")) {
					dbEvent.setDescription(description);
				}
				if(joined!= -1) {
					dbEvent.setJoined(1);
				}
				else{
					dbEvent.setJoined(0);
				}
				events.add(dbEvent);
			}
		}
		return events;
	}

	/**
	 * Checks if is sets the field(use in filter).
	 *
	 * @param dateFrom the date from
	 * @return true, if is sets the
	 */
	private boolean isSet(Date dateFrom) {
		return dateFrom != null;
	}

	/**
	 * Checks if is sets the field(use in filter).
	 *
	 * @param type the type
	 * @return true, if is sets the
	 */
	private boolean isSet(String type) {
		return type != null && type != "";
	}
	
	/**
	 * Update event in datatable.
	 *
	 * @param event event to update
	 * @throws SQLException the SQL exception
	 */
	
	public void updateEvent(Event event) throws SQLException {
		Database database = Database.getInstance();
		StringBuffer query = new StringBuffer("UPDATE " + RELATION + " SET ");
		query.append(EventsEnum.CITY + "=?, ");
		query.append(EventsEnum.DATE + "=?, ");
		query.append(EventsEnum.DESCRIPTION + "=?, ");
		query.append(EventsEnum.GAME + "=?, ");
		query.append(EventsEnum.LIMIT + "=?, ");
		query.append(EventsEnum.NAME + "=?, ");
		query.append(EventsEnum.STREET + "=?, ");
		query.append(EventsEnum.TYPE + "=? WHERE ");
		query.append(EventsEnum.ID + "=?");
		PreparedStatement preparedStatement;
		if(database == null) {
			database = Database.getInstance();
		} 
		preparedStatement = database.getConnection().prepareStatement(query.toString());
		preparedStatement.setString(1, event.getCity());
		preparedStatement.setTimestamp(2, new java.sql.Timestamp(event.getDateEvent().getTime()));
		preparedStatement.setString(3, event.getDescription());
		preparedStatement.setString(4, event.getGame());
		preparedStatement.setInt(5, event.getPlayersLimit());
		preparedStatement.setString(6, event.getName());
		preparedStatement.setString(7, event.getStreet());
		preparedStatement.setString(8, event.getType());
		preparedStatement.setInt(9, event.getID());
		preparedStatement.executeUpdate();
	}

	/**
	 * Gets the the event.
	 *
	 * @param id id of the event
	 * @return the the event
	 * @throws SQLException the SQL exception
	 */
	public Event getTheEvent(int id) throws SQLException {
		Event dbEvent = null;
		Database database = Database.getInstance();
		StringBuffer query = new StringBuffer("SELECT * FROM " + RELATION + " WHERE ID=?");
		PreparedStatement preparedStatement;
		if(database == null) {
			database = Database.getInstance();
		} 
		preparedStatement = database.getConnection()
				.prepareStatement(query.toString());
		preparedStatement.setInt(1, id);
		rs = preparedStatement.executeQuery();
		String name, street, game, description, user_login, city, type;
		int playersLimit;
		Date dateFrom;
		if(rs != null){
			while (rs.next()) {
				name = rs.getString(EventsEnum.NAME.toString());
				type = rs.getString(EventsEnum.TYPE.toString());
				playersLimit = rs.getInt(EventsEnum.LIMIT.toString());
				city = rs.getString(EventsEnum.CITY.toString());
				street = rs.getString(EventsEnum.STREET.toString());
				game = rs.getString(EventsEnum.GAME.toString());
				description = rs.getString(EventsEnum.DESCRIPTION.toString());
				user_login = rs.getString(EventsEnum.ADMIN.toString());
				dateFrom = new Date(rs.getTimestamp(EventsEnum.DATE.toString()).getTime());
				
				dbEvent = new Event(name, type, playersLimit, dateFrom, city, user_login);
				dbEvent.setID(id);
				if(isSet(street) && !street.equals("NULL")) {
					dbEvent.setStreet(street);
				}
				if(isSet(game) && !game.equals("NULL")) {
					dbEvent.setGame(game);
				}
				if(isSet(description) && !description.equals("NULL")) {
					dbEvent.setDescription(description);
				}
			}
		}
		return dbEvent;
	}

}
