package databaseEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The Class ClearDatabase.
 */
public class ClearDatabase {

	/** The event entity name. */
	private static String EVENT_RELATION = "gamika.events";
	
	/** The users entity name. */
	private static String USERS_RELATION = "gamika.users";
	
	/** The game entity name. */
	private static String GAMES_RELATION = "gamika.games";
	
	/** The usersevents relation. */
	private static String USERSEVENTS_RELATION = "gamika.usersevents";
	
	/** The stmt. */
	private java.sql.Statement stmt = null;

	/** The rs. */
	private ResultSet rs = null;
	
	/**
	 * Instantiates a new clear database, delete all rows in datatable.
	 *
	 * @throws SQLException the SQL exception
	 */
	public ClearDatabase() throws SQLException {
		Database database = Database.getInstance();

		if(database == null) {
			database = Database.getInstance();
		} 
		stmt = database.getConnection().createStatement();
		stmt.executeUpdate("DELETE FROM " + USERSEVENTS_RELATION + "; ");
		stmt.executeUpdate("DELETE FROM " + GAMES_RELATION + "; ");
		stmt.executeUpdate("DELETE FROM " + EVENT_RELATION + "; ");
		stmt.executeUpdate("DELETE FROM " + USERS_RELATION + "; ");
		
	}
}
