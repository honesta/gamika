package databaseEntity;


/**
 * The Enum to store name of columns of entity USERS.
 */
public enum UsersEnum {
	
	/** The login of user. */
	LOGIN {

		public String toString() {
			return "login";
		}
	},

	/** The password of user. */
	PASSWORD {

		public String toString() {
			return "password";
		}
	},

	/** The salt to secure a password. */
	SALT {

		public String toString() {
			return "salt";
		}
	},

	/** The email of user. */
	EMAIL {

		public String toString() {
			return "email";
		}
	},

	/** The name of user. */
	NAME {

		public String toString() {
			return "name";
		}
	},
	
	/** The surname. */
	SURNAME {

		public String toString() {
			return "surname";
		}
	},

	/** The city. */
	CITY {

		public String toString() {
			return "city";
		}
	},

	/** The photo. */
	PHOTO {

		public String toString() {
			return "photo";
		}
	},

}
