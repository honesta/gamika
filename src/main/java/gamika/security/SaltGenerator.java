package gamika.security;

import java.util.Random;

/**
 * The Class SaltGenerator to generate random salt for password.
 */
public class SaltGenerator {

	/** The instance of class. */
	private static SaltGenerator instance;
	private Random random;
	
	/**
	 * Instantiates a new salt generator.
	 */
	SaltGenerator() {
		random = new Random();
	}
	
	static void setInstance(SaltGenerator instance) {
		SaltGenerator.instance = instance;
	}
	
	/**
	 * Gets the single instance of SaltGenerator.
	 *
	 * @return single instance of SaltGenerator
	 */
	public static SaltGenerator getInstance() {
		if (instance == null) 
			instance = new SaltGenerator();
		return instance;
	}
	
	/**
	 * Generate salt.
	 *
	 * @param length the length
	 * @return the byte[]
	 */
	public byte[] generateSalt(int length) {
		byte[] salt = new byte[length];
		
		for (int i = 0; i < length; i++) {
			salt[i] = (byte) random.nextInt(256);
		}
		
		return salt;
	}
	
}
