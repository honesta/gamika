package gamika.security;

import java.util.Arrays;

/**
 * The Class Password to store password and salt of user.
 */
public class Password {

	/** The hash. */
	private byte[] hash;
	
	/** The salt. */
	private byte[] salt;
	
	/**
	 * Instantiates a new password.
	 *
	 * @param hash the hash
	 * @param salt the salt
	 */
	public Password(byte[] hash, byte[] salt) {
		this.hash = hash;
		this.salt = salt;
	}

	/**
	 * Gets the hash.
	 *
	 * @return the hash
	 */
	public byte[] getHash() {
		return hash;
	}

	/**
	 * Gets the salt.
	 *
	 * @return the salt
	 */
	public byte[] getSalt() {
		return salt;
	}
	
	/**
	 * Gets the hash string.
	 *
	 * @return the hash string
	 */
	public String getHashString() {
		return new String(hash);
	}
	
	/**
	 * Gets the salt string.
	 *
	 * @return the salt string
	 */
	public String getSaltString() {
		return new String(salt);
	}

	/**
	 * Method to compare class Password.
	 *
	 * @param otherPassword the other password
	 * @return true, if successful
	 */
	public boolean equals(Password otherPassword) {
		byte[] otherHash = otherPassword.getHash();
		byte[] otherSalt = otherPassword.getSalt();
		
		if ((hash.length != otherHash.length) || (salt.length != otherSalt.length))
			return false;
		
		if (Arrays.equals(salt, otherSalt) && Arrays.equals(hash, otherHash))
			return true;
		else
			return false;
	}
	
	
}
