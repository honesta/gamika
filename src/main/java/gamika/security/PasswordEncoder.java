package gamika.security;

/**
 * The Interface PasswordEncoder.
 */
public interface PasswordEncoder {
	
	/**
	 * Encode.
	 *
	 * @param password the password
	 * @return the password
	 */
	Password encode(String password);
	
	/**
	 * Encode.
	 *
	 * @param password the password
	 * @param salt the salt
	 * @return the password
	 */
	Password encode(String password, byte[] salt);
}
