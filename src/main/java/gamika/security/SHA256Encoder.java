package gamika.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * The Class SHA256Encoder.
 */
public class SHA256Encoder implements PasswordEncoder {
	
	/* (non-Javadoc)
	 * @see gamika.security.PasswordEncoder#encode(java.lang.String)
	 */
	@Override
	public Password encode(String password) {
		SaltGenerator saltGenerator = SaltGenerator.getInstance();
		byte[] salt = saltGenerator.generateSalt(32);
		return encode(password, salt);
	}
	
	/* (non-Javadoc)
	 * @see gamika.security.PasswordEncoder#encode(java.lang.String, byte[])
	 */
	@Override
	public Password encode(String password, byte[] salt) {
		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
			messageDigest.update(password.getBytes());
			messageDigest.update(salt);
			return new Password(messageDigest.digest(), salt);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

}
