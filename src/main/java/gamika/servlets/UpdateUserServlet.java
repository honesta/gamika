package gamika.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import databaseData.User;
import databaseEntity.DatabaseException;
import databaseEntity.UserAlreadyExistsException;
import databaseEntity.Users;
import gamika.security.SHA256Encoder;

/**
 * The Class CreateUserServlet.
 */
public class UpdateUserServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 11L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession(false);

		if (session != null) {
			String userLogin = (String) session.getAttribute("Username");
			if (userLogin != null) {
				String responseString = "{ \"status\": \"%s\" }";
				PrintWriter writer = resp.getWriter();
		
				String email = req.getParameter("email");
				System.out.println(email);
				String name = req.getParameter("name");
				System.out.println(name);
				String surname = req.getParameter("surname");
				System.out.println(surname);
				String city = req.getParameter("city");
				System.out.println(city);
				User user = null;
				if(userLogin != "" && email != "") {
					user = new User(userLogin, null, email);
					user.setName(name);
					user.setSurname(surname);
					user.setCity(city);
				}
				if(user != null){
					try {
						Users.getInstance().updateUser(user);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					writer.println(String.format(responseString, "registered"));
					
				}
				else {
					writer.println(String.format(responseString, "incorectData"));
				}
			}
		}
	}

}
