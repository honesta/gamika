package gamika.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import databaseData.Event;
import databaseData.EventFilter;
import databaseEntity.Events;

/**
 * The Class GetEventsServlet to filter events.
 */
public class GetEventsServlet extends HttpServlet {

	private static final long serialVersionUID = 662482319106126767L;

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String responseString = "{ \"status\": \"%s\" }";
		HttpSession session = req.getSession(false);
		resp.setCharacterEncoding("UTF-8");
		PrintWriter writer = resp.getWriter();
		if (session != null) {
		try {
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			String type = req.getParameter("type");
			String dateFromString = req.getParameter("dateEventFrom");
			String dateToString = req.getParameter("dateEventTo");
			String city = req.getParameter("city");
			System.out.println(dateFromString);
			Date dateFrom = null;
			if (dateFromString != null && !dateFromString.equals(""))
				dateFrom = dateFormat.parse(dateFromString);
			
			Date dateTo = null;
			if (dateToString != null && !dateToString.equals(""))
				dateTo = dateFormat.parse(dateToString);
			
			EventFilter eventFilter = new EventFilter();
			eventFilter.setCity(city);
			eventFilter.setDateFrom(dateFrom);
			eventFilter.setDateTo(dateTo);
			eventFilter.setType(type);
			eventFilter.setLogin((String) req.getSession().getAttribute("Username"));
			
			ArrayList<Event> events = Events.getInstance().getEvents(eventFilter); 
			
			writer.println(String.format("{ \"count\": %d, \"event\": [", events.size()));
			
			for (int i = 0; i < events.size(); i++) {
				Event event = events.get(i);
				writer.print(String.format("{ \"name\": \"%s\", \"owner\": \"%s\", \"game\": \"%s\", \"dateEvent\": \"%s\", \"hour\": \"%s\", \"type\": \"%s\", \"city\": \"%s\", \"eventID\": \"%s\", \"joined\": \"%s\" }", event.getName(), event.getOwnerLogin(), event.getGame(), event.getDateEvent().toString(), "godzina", event.getType(), event.getCity(),event.getID(),event.getJoinedString()));
				if (i < events.size() - 1)
					writer.println(",");
			}
			
			writer.println("] }");
		} catch (ParseException | SQLException exception) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
			exception.printStackTrace(System.out);
		}
		} else {
			writer.println(String.format(responseString, "userNotLoggedIn"));			
		}
	}

}
