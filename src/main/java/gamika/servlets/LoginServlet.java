package gamika.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import databaseEntity.Users;
import gamika.security.Password;
import gamika.security.SHA256Encoder;

/**
 * The Class LoginServlet.
 */
public class LoginServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -629611993823753417L;

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String login = req.getParameter("login");
		String password = req.getParameter("password");
		String responseString = "{ \"status\": \"%s\" }";
		PrintWriter writer = resp.getWriter();

		try {
			Password usersPassword = Users.getInstance().getPassword(login);
			SHA256Encoder encoder = new SHA256Encoder();
			
			if (usersPassword != null) {
				Password receivedPassword = encoder.encode(password, usersPassword.getSalt());
				if (usersPassword.equals(receivedPassword)) {
					HttpSession session = req.getSession();
					session.setAttribute("Username", login);
					writer.println(String.format(responseString, "loggedIn"));
				} else {
					writer.println(String.format(responseString, "incorrectData"));
				}
			} else {
				writer.println(String.format(responseString, "incorrectData"));
			}

		} catch (SQLException e) {
			writer.println(String.format(responseString, "databaseError"));
		}
	}

}
