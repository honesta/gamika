package gamika.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import databaseData.Event;
import databaseEntity.ClearDatabase;
import databaseEntity.Events;

public class ClearDatabaseServlet extends HttpServlet {

	private static final long serialVersionUID = -8640519558368652374L;

	/**
	 * Create new event 
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String responseString = "{ \"status\": \"%s\" }";
		PrintWriter writer = resp.getWriter();
		HttpSession session = req.getSession(false);
	
		try {
			ClearDatabase clearDatabase = new ClearDatabase();
			writer.println(String.format(responseString, "deleted"));
		} catch (SQLException e) {
			System.out.println("sql error " + e.getMessage());
			writer.println(String.format(responseString, "notdeleted"));
		}
		
	}
}
