package gamika.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import databaseData.Event;
import databaseData.EventFilter;
import databaseData.User;
import databaseEntity.Events;
import databaseEntity.Users;

/**
 * The Class UpdateEventServlet to update event.
 */
public class UpdateEventServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3024432935121455935L;
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String responseString = "{ \"status\": \"%s\" }";
		PrintWriter writer = resp.getWriter();
		HttpSession session = req.getSession(false);

		if (session != null) {
			String name = req.getParameter("name");
			String type = req.getParameter("type");
			String description = req.getParameter("description");
			String playersLimitString = req.getParameter("playersLimit");
			int playersLimit = 0;
			
			try {
				playersLimit = Integer.parseInt(playersLimitString);
			} catch (NumberFormatException e) {
				writer.println(String.format(responseString, "dateFormatError"));
				return;
			}
			
			String city = req.getParameter("city");
			String street = req.getParameter("street");
			String game = req.getParameter("game");
			String ownerLogin = (String) req.getSession().getAttribute("Username");
			String idString = req.getParameter("eventID");
			int id = -1;
			try {
				id = Integer.parseInt(idString);
			} catch (NumberFormatException e) {
				writer.println(String.format(responseString, "dateFormatError"));
				return;
			}
			Date dateEvent = null;
			DateFormat format = new SimpleDateFormat("MM/dd/yyyy/HH:mm");
			
			try {
				String data = req.getParameter("dateEvent");
				dateEvent = format.parse(data);
			} catch (ParseException e) {
				writer.println(String.format(responseString, "dateFormatError"));
				return;
			}
			System.out.println(name);
			System.out.println(type);
			System.out.println(city);
			System.out.println(ownerLogin);
			if(name != "" && type != ""  && city != "" && ownerLogin != "") {
				Event event = new Event(name, type, playersLimit, dateEvent, city, ownerLogin);
				if(description != null && description != "") {
					event.setDescription(description);
				}
				if(game != null && game != "") {
					event.setGame(game);
				}
				if(street != null && street != "") {
					event.setStreet(street);
				}
				event.setID(id);
			
				try {
					Events.getInstance().updateEvent(event);
					writer.println(String.format(responseString, "updated"));
				} catch (SQLException e) {
					System.out.println("in catch");
					e.printStackTrace(System.err);
					writer.println(String.format(responseString, "databaseError"));
				}
			}
			else {
				writer.println(String.format(responseString, "databaseError"));
			}
				
		} else {
			writer.println(String.format(responseString, "userNotLoggedIn"));			
		}
	}

}
