package gamika.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import databaseData.User;
import databaseEntity.Users;

/**
 * The Class GetUserDataServlet to get info about logged user.
 */
public class GetUserDataServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3024432935121455934L;
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession(false);
		if (session != null) {
			
			String userLogin = (String) session.getAttribute("Username");
			if (userLogin != null) {
				User userData = null;
				try {
					userData = Users.getInstance().getInfoAboutUser(userLogin);
					resp.setCharacterEncoding("UTF-8");
				} catch (SQLException e) {
					e.printStackTrace();
					resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
				String response = String.format("{ \"name\": \"%s\", \"surname\": \"%s\", \"city\": \"%s\", \"email\": \"%s\" }", userData.getName(), userData.getSurname(), userData.getCity(), userData.getEmail());
				resp.getWriter().println(response);
			} else {
				resp.sendError(HttpServletResponse.SC_FORBIDDEN);
			}
		} else {
			resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}		
	}

}
