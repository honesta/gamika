/**
 * 
 */
package gamika.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import databaseData.Event;
import databaseData.Game;
import databaseEntity.Events;
import databaseEntity.Games;

/**
 * @author ppp
 *
 */
public class CreateGameServlet extends HttpServlet{
	
	private static final long serialVersionUID = -8640519558368552374L;
	/**
	 * Create new event 
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String responseString = "{ \"status\": \"%s\" }";
		PrintWriter writer = resp.getWriter();
		HttpSession session = req.getSession(false);

		if (session != null) {
			String name = req.getParameter("name");
			String language = req.getParameter("language");
			String rules = req.getParameter("rules");
			String playersLimitString = req.getParameter("playersLimit");
			int playersLimit = 0;
			
			if(  (playersLimitString!= null) && (playersLimitString.length()>0)){
				try {
					playersLimit = Integer.parseInt(playersLimitString);
				} catch (NumberFormatException e) {
					writer.println(String.format(responseString, "dateFormatError"));
					return;
				}
			}
			int version = 0;
			String versionString = req.getParameter("version");
			if(  (versionString!= null) && (versionString.length()>0)){
				try {
					version = Integer.parseInt(versionString);
				} catch (NumberFormatException e) {
					writer.println(String.format(responseString, "dateFormatError"));
					return;
				}
			}
			int PEGI = 0;
			String PEGIString = req.getParameter("PEGI");
			if(  (PEGIString!= null) && (PEGIString.length()>0)){
				try {
					PEGI = Integer.parseInt(PEGIString);
				} catch (NumberFormatException e) {
					writer.println(String.format(responseString, "dateFormatError"));
					return;
				}
			}
			String author = req.getParameter("author");
			String type = req.getParameter("type");
			String ownerLogin = (String) req.getSession().getAttribute("Username");

			
			if(name != "" && version != 0 && ownerLogin != "" && language != "") {
				Game game = new Game(name,version,language);
				if(rules != null && rules.length()>0) {
					game.setRules(rules);
				}
				if(playersLimit != 0 ) {
					game.setPlayersLimit(playersLimit);
				}
				if(PEGI != 0){
					game.setPEGI(PEGI);
				}
				if(author != null && author.length()>0) {
					game.setAuthor(author);
				}
				if(type != null && type.length()>0 ) {
					game.setType(type);
				}
				try {
					Games.getInstance().addGame(game);
					writer.println(String.format(responseString, "added"));
				} catch (SQLException e) {
					System.out.println("sql error " + e.getMessage());
					writer.println(String.format(responseString, "databaseError"));
				}
			}
			else {
				writer.println(String.format(responseString, "databaseError"));
			}
				
		} else {
			writer.println(String.format(responseString, "userNotLoggedIn"));			
		}
	}
}
