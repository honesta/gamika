package gamika.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import databaseData.Event;
import databaseData.UserEvent;
import databaseEntity.DatabaseException;
import databaseEntity.Events;
import databaseEntity.UserAlreadyExistsException;
import databaseEntity.UserEventAlreadyExistsException;
import databaseEntity.UsersEvents;

public class CreateUserEventServlet extends HttpServlet {
	private static final long serialVersionUID = -8640519558368552374L;
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String responseString = "{ \"status\": \"%s\" }";
		PrintWriter writer = resp.getWriter();
		HttpSession session = req.getSession(false);

		if (session != null) {
			String login = (String) req.getSession().getAttribute("Username");
			String eventIDString = req.getParameter("eventID");
			int eventID = -1;
			
			try {
				eventID = Integer.parseInt(eventIDString);
			} catch (NumberFormatException e) {
				writer.println(String.format(responseString, "dateFormatError"));
				return;
			}
			if(login != "" && eventID != -1 ) {
				UserEvent userEvent = new UserEvent(login,eventID);
				try {
					UsersEvents.getInstance().addUserEvent(userEvent);
					writer.println(String.format(responseString, "joined"));
				} catch (UserEventAlreadyExistsException e) {
					writer.println(String.format(responseString, "alredyExists"));
				} catch (DatabaseException e) {
					System.out.println("sql error " + e.getMessage());
					writer.println(String.format(responseString, "databaseError"));
				}
			}
			else {
				writer.println(String.format(responseString, "databaseError"));
			}
				
		} else {
			writer.println(String.format(responseString, "userNotLoggedIn"));			
		}
	}
}
