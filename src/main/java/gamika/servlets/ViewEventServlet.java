package gamika.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import databaseData.Event;
import databaseEntity.Events;

/**
 * The Class ViewEventServlet to view the event.
 */
public class ViewEventServlet extends HttpServlet {

	private static final long serialVersionUID = 662482319106127767L;

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String responseString = "{ \"status\": \"%s\" }";
		HttpSession session = req.getSession(false);
		resp.setCharacterEncoding("UTF-8");
		PrintWriter writer = resp.getWriter();
		if (session != null) {
		try {
			
			int id = Integer.parseInt(req.getParameter("eventID"));
			Event event = Events.getInstance().getTheEvent(id); 
			DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm");
			String reportDate = df.format(event.getDateEvent());
			
			writer.print(String.format("{ \"name\": \"%s\", \"owner\": \"%s\", \"game\": \"%s\", \"dateEvent\": \"%s\", \"hour\": \"%s\", \"type\": \"%s\", \"city\": \"%s\", \"eventID\": \"%s\", \"description\": \"%s\", \"street\": \"%s\", \"playersLimit\": \"%s\" }", event.getName(), event.getOwnerLogin(), event.getGame(), reportDate, "", event.getType(), event.getCity(),event.getID(), event.getDescription(), event.getStreet(), event.getPlayersLimit()));
			
		} catch (SQLException exception) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
			exception.printStackTrace(System.out);
		}
		} else {
			writer.println(String.format(responseString, "userNotLoggedIn"));			
		}
	}

}
