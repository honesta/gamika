package gamika.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import databaseData.Event;
import databaseData.EventFilter;
import databaseData.Game;
import databaseData.GameFilter;
import databaseEntity.Events;
import databaseEntity.Games;

public class GetGamesServlet extends HttpServlet {
	private static final long serialVersionUID = 662482319106126767L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
		try {
			resp.setCharacterEncoding("UTF-8");	
			PrintWriter writer = resp.getWriter();

			String type = req.getParameter("type");
			String language = req.getParameter("language");
			
			GameFilter gameFilter = new GameFilter();
			gameFilter.setType(type);
			gameFilter.setLanguage(language);
			
			ArrayList<Game> games = Games.getInstance().getGames(gameFilter);
			writer.println(String.format("{ \"count\": %d, \"game\": [", games.size()));
			
			for (int i = 0; i < games.size(); i++) {
				Game game = games.get(i);
				writer.print(String.format("{ \"name\": \"%s\", \"version\": \"%s\", \"language\": \"%s\", \"type\": \"%s\", \"PEGI\": \"%s\"  }",game.getName(), game.getVersion(), game.getLanguage() , game.getType(), game.getPEGI()));
				if (i < games.size() - 1)
					writer.println(",");
			}
			
			writer.println("] }");
		} catch (ParseException | SQLException exception) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
			exception.printStackTrace(System.out);
		}
	}
}
