package gamika.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import databaseData.User;
import databaseEntity.DatabaseException;
import databaseEntity.UserAlreadyExistsException;
import databaseEntity.Users;
import gamika.security.SHA256Encoder;

/**
 * The Class CreateUserServlet.
 */
public class CreateUserServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String login = req.getParameter("login");
		String password = req.getParameter("password");
		String email = req.getParameter("email");
		String name = req.getParameter("name");
		String surname = req.getParameter("surname");
		String city = req.getParameter("city");
		String responseString = "{ \"status\": \"%s\" }";
		PrintWriter writer = resp.getWriter();
		if(login != "" && password != "" && email != "") {
			User user = new User(login, new SHA256Encoder().encode(password), email);
			if(name != null && name != "") {
				user.setName(name);
			}
			if(surname != null && surname != "") {
				user.setSurname(surname);
			}
			if(city != null && city != "") {
				user.setCity(city);
			}
			
			
			
			try {
				Users.getInstance().addUser(user);
				writer.println(String.format(responseString, "registered"));
			} catch (UserAlreadyExistsException e) {
				writer.println(String.format(responseString, "alredyExists"));
			} catch (DatabaseException e) {
				writer.println(String.format(responseString, "databaseError"));
			}
		}
		else {
			writer.println(String.format(responseString, "databaseError"));
		}
	}

}
