Testy wykonane z wykorzystaniem narzędzia JMeter 
1. logowanie_gry - test ten uwzględniał logowanie i wyświetlenie wszystkich gier
2. logowanie_wydarzenia - test uwzględniał logowanie i wyświetlenie listy wydarzeń
3. logowanie_wydarzenia_filtr_info - test ten uwzględniał logowanie, wyświetlenie listy wydarzeń, 
przefiltrowanie jej i wyświetlenie informacji o jednym wydarzeniu

Wymagania:
 W bazie danych istnieje użytkownik:
  login Poprawny
  hasło Poprawny
 W bazie danych istnieje event o id =1

