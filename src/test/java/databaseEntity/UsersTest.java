package databaseEntity;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import databaseData.User;
import gamika.security.Password;
import gamika.security.SHA256Encoder;

import static org.mockito.Mockito.*;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsersTest {

	private Database database;
	private Connection connection;
	private PreparedStatement preparedStatement;
	private java.sql.Statement statement;
	private Users users;
	
	private ResultSet resultSet;
	String email = "email1";
	String city = "city1";
	String surname = "surname1";
	String name = "name1";
	String login = "login1";
	byte[] salt = { 's', 'a', 'l', 't' };
	byte[] hash = { 'h', 'a', 's', 'h' };
	
	@Before
	public void setUp() throws Exception {
		database = mock(Database.class);
		Database.setInstance(database);
		connection = mock(Connection.class);
		preparedStatement = mock(PreparedStatement.class);
		statement = mock(java.sql.Statement.class);
		resultSet = mock(ResultSet.class);
		users = new Users();
		when(database.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
		when(connection.createStatement()).thenReturn(statement);
		when(statement.executeQuery(anyString())).thenReturn(resultSet);
		when(resultSet.next()).thenReturn(true);
		when(resultSet.getBytes("password")).thenReturn(hash);
		when(resultSet.getBytes("salt")).thenReturn(salt);		
		when(resultSet.getString("email")).thenReturn(email);
		when(resultSet.getString("city")).thenReturn(city);
		when(resultSet.getString("surname")).thenReturn(surname);
		when(resultSet.getString("name")).thenReturn(name);
	}

	@Test(expected=UserAlreadyExistsException.class)
	public void testAddUser_addUsersWithEqualLogin() throws UserAlreadyExistsException, DatabaseException, SQLException {
		when(preparedStatement.executeUpdate()).thenReturn(1)
									   .then(new Answer<Object>() {
											@Override
											public Object answer(InvocationOnMock invocation) throws Throwable {
												throw new SQLException("Already exists", "state", 1062);
											}
									   });
		users.addUser(new User(login, new SHA256Encoder().encode("password"), email));
		users.addUser(new User(login, new SHA256Encoder().encode("password"), email));
	}	
	
	@Test
	public void testAddUser_testSqlQuery() throws UserAlreadyExistsException, DatabaseException, SQLException {
		users.addUser(new User(login, new SHA256Encoder().encode("password"), email));
		verify(connection, times(1)).prepareStatement("INSERT INTO gamika.users(login,password,salt,email) VALUES (?,?,?,?)");
	}
	
	@Test
	public void testGetPassword() throws SQLException {
		//Password p = Users.getPassword(login);
		//assertTrue(Users.getSqlQuery().equals("SELECT password, salt FROM gamika.users WHERE login='"+login+"'"));
		Password password = users.getPassword(login);
		verify(statement, times(1)).executeQuery("SELECT password, salt FROM gamika.users WHERE login='" + login + "'");
		verify(resultSet, times(1)).getBytes("password");
		verify(resultSet, times(1)).getBytes("salt");
		assertArrayEquals(salt, password.getSalt());
		assertArrayEquals(hash, password.getHash());
	}
		
	@Test
	public void testGetInfoAboutUser_testSqlQuery() throws SQLException {
		users.getInfoAboutUser(login);
		verify(statement, times(1)).executeQuery("SELECT email, name, surname, city FROM gamika.users WHERE login='" + login + "'");
	}
	
	@Test
	public void testGetInfoAboutUser_testReturnedData() throws SQLException {		
		User user = users.getInfoAboutUser(login);
		assertEquals(city, user.getCity());
		assertEquals(email, user.getEmail());
		assertEquals(surname, user.getSurname());
		assertEquals(name, user.getName());
		assertEquals(login, user.getLogin());	
	}
	
	
	
	
}
