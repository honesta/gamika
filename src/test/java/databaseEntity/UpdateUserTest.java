package databaseEntity;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import databaseData.User;
import gamika.security.SHA256Encoder;

public class UpdateUserTest {

	private Database database;
	private Connection connection;
	private PreparedStatement preparedStatement;
	private java.sql.Statement statement;
	private Users users;
	
	private ResultSet resultSet;
	String email = "email1";
	String city = "city1";
	String surname = "surname1";
	String name = "name1";
	String login = "login1";
	byte[] salt = { 's', 'a', 'l', 't' };
	byte[] hash = { 'h', 'a', 's', 'h' };
	
	@Before
	public void setUp() throws Exception {
		database = mock(Database.class);
		Database.setInstance(database);
		connection = mock(Connection.class);
		preparedStatement = mock(PreparedStatement.class);
		statement = mock(java.sql.Statement.class);
		resultSet = mock(ResultSet.class);
		users = new Users();
		when(database.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
		when(connection.createStatement()).thenReturn(statement);
		when(statement.executeQuery(anyString())).thenReturn(resultSet);
		when(resultSet.next()).thenReturn(true);
		when(resultSet.getBytes("password")).thenReturn(hash);
		when(resultSet.getBytes("salt")).thenReturn(salt);		
		when(resultSet.getString("email")).thenReturn(email);
		when(resultSet.getString("city")).thenReturn("cityUpdated");
		when(resultSet.getString("surname")).thenReturn("surnameUpdated");
		when(resultSet.getString("name")).thenReturn("nameUpdated");
	}
	
	@Test
	public void testUpdateUser_testSqlQuery() throws SQLException {
		users.updateUser(new User(login, new SHA256Encoder().encode("password"), email));
		verify(connection, times(1)).prepareStatement("UPDATE gamika.users SET city=?, email=?, name=?, surname=? WHERE login=?");
	}
	
	@Test
	public void testUpdateUser_testReturnedData() throws SQLException {
		User user = users.getInfoAboutUser(login);
		user.setCity("cityUpdated");
		user.setSurname("surnameUpdated");
		user.setName("nameUpdated");
		users.updateUser(user);
		user = users.getInfoAboutUser(login);
		assertEquals("cityUpdated", user.getCity());
		assertEquals(email, user.getEmail());
		assertEquals("surnameUpdated", user.getSurname());
		assertEquals("nameUpdated", user.getName());	
	}
}
