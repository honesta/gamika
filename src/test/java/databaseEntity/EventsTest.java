package databaseEntity;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import databaseData.Event;
import databaseData.EventFilter;

import static org.mockito.Mockito.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.sql.Date;

public class EventsTest {

	private Database database;
	private Connection connection;
	private PreparedStatement statement;
	private java.sql.Statement stmt;
	private Events events;
	private ResultSet rs;
	
	private String name = "name1";
	private String type = "type1";
	private int playersLimit = 1;
	private Date dateEvent = new Date(12);
	private String city = "city1";
	private String ownerLogin = "ownerLogin1";
	
	@Before
	public void setUp() throws Exception {
		database = mock(Database.class);
		connection = mock(Connection.class);
		statement = mock(PreparedStatement.class);
		stmt = mock(java.sql.Statement.class);
		rs = mock(ResultSet.class);
		Database.setInstance(database);

		when(database.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(anyString())).thenReturn(statement);
		when(connection.createStatement()).thenReturn(stmt);
		when(stmt.executeQuery(anyString())).thenReturn(rs);
		when(rs.next()).thenReturn(true);
		when(rs.getString("name")).thenReturn(name);
		when(rs.getString("type")).thenReturn(type);
		when(rs.getString("city")).thenReturn(city);
		when(rs.getString("ownerLogin")).thenReturn(ownerLogin);
		when(rs.getDate("dateEvent")).thenReturn(dateEvent);
		when(rs.getInt("playersLimit")).thenReturn(playersLimit);
		
		events = new Events();
	}
	
	@Test
	public void testAddEvent_testSqlQuery() throws SQLException {
		events.addEvent(new Event(name,type,playersLimit,dateEvent,city,ownerLogin));
		verify(connection, times(1)).prepareStatement("INSERT INTO gamika.events(name,type,players_limit,date_event,user_login,city) VALUES ('name1','type1',1, ?,'ownerLogin1','city1')");
	}
	
	@Test 
	public void testGetEvents_testSqlQuery() throws SQLException, ParseException {
		EventFilter filter = new EventFilter();
		filter.setCity(city);
		filter.setDateFrom(dateEvent);
		filter.setType(type);
		events.getEvents(filter);
		verify(connection, times(1)).prepareStatement("SELECT * FROM gamika.events left join (select * from gamika.usersevents where usersevents.user_login like 'null') usersevents on gamika.events.id = usersevents.event_id WHERE type LIKE '%type1%' AND city LIKE '%city1%' AND date_event > ? AND TRUE");
	}
	
	@Test
	public void testUdateEvent_testSqlQuery() throws SQLException {
		Event event = new Event(name,type,playersLimit,dateEvent,city,ownerLogin);
		event.setDescription("description");
		event.setGame("game");
		event.setStreet("street");
		events.updateEvent(event);
		verify(connection, times(1)).prepareStatement("UPDATE gamika.events SET city=?, date_event=?, description=?, game=?, players_limit=?, name=?, street=?, type=? WHERE ID=?");
	}

	@Test
	public void testGetTheEvent_testSqlQuery() throws SQLException {
		Event event = new Event(name,type,playersLimit,dateEvent,city,ownerLogin);
		event.setID(1);
		events.addEvent(event);
		events.getTheEvent(1);
		verify(connection, times(1)).prepareStatement("SELECT * FROM gamika.events WHERE ID=?");
	}
	
}
