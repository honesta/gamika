package gamika.security;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PasswordTest {

	private static final byte[] salt = {40, 60, 10, 20};
	private static final byte[] hash = {100, 10, 50, 120};
	
	private static final byte[] otherSalt = {20, 10, 60, 40};
	private static final byte[] otherHash = {120, 50, 10, 100};
	
	private static final byte[] saltWithDifferentLength = {40, 60, 10, 20, 40};
	private static final byte[] hashWithDifferentLength = {40, 60, 10, 20, 40};
	
	private Password password;

	@Before
	public void setUp() throws Exception {
		password = new Password(hash, salt);
	}

	@Test
	public void testGetHash() {
		assertEquals(hash, password.getHash());
	}

	@Test
	public void testGetSalt() {
		assertEquals(salt, password.getSalt());
	}

	@Test
	public void testGetHashString() {
		assertEquals(new String(hash), password.getHashString());
	}

	@Test
	public void testGetSaltString() {
		assertEquals(new String(salt), password.getSaltString());
	}

	@Test
	public void testEqualsPassword_twoEqualPasswords() {
		Password otherPassword = new Password(hash, salt);
		assertTrue(password.equals(otherPassword));
	}
	
	@Test
	public void testEqualsPassword_passwordsWithDifferentHash() throws Exception {
		Password otherPassword = new Password(otherHash, salt);
		assertFalse(password.equals(otherPassword));
	}
	
	@Test
	public void testEqualsPassword_passwordsWithDifferentSalt() throws Exception {
		Password otherPassword = new Password(hash, otherSalt);
		assertFalse(password.equals(otherPassword));
	}

	@Test
	public void testEqualsPassword_twoDifferentPasswords() throws Exception {
		Password otherPassword = new Password(otherHash, otherSalt);
		assertFalse(password.equals(otherPassword));
	}
	
	@Test
	public void testEqualsPassword_passwordsWithDifferentHashLength() throws Exception {
		Password otherPassword = new Password(hashWithDifferentLength, salt);
		assertFalse(password.equals(otherPassword));
	}
	
	@Test
	public void testEqualsPassword_passwordsWithDifferentSaltLength() throws Exception {
		Password otherPassword = new Password(hash, saltWithDifferentLength);
		assertFalse(password.equals(otherPassword));
	}
	
}
