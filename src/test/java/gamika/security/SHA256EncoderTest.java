package gamika.security;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class SHA256EncoderTest {

	private static final byte[] salt = new byte[] { 1, 2 };
	private static final byte[] otherSalt = new byte[] { 3, 4 };
	
	private static final String passwordString = "password";
	private static final String otherPasswordString = "otherPassword";
	
	private SaltGenerator saltGenerator;
	private SHA256Encoder encoder;

	@Before
	public void setUp() throws Exception {
		saltGenerator = mock(SaltGenerator.class);
		SaltGenerator.setInstance(saltGenerator);
		encoder = new SHA256Encoder();
	}

	@Test
	public void testEncodeString_generatePasswordsWithDifferentSalts() {
		when(saltGenerator.generateSalt(32)).thenReturn(salt)
											.thenReturn(otherSalt);
		Password password = encoder.encode(passwordString);
		Password otherPassword = encoder.encode(passwordString);
		assertFalse(password.equals(otherPassword));
		verify(saltGenerator, times(2)).generateSalt(32);
	}

	@Test
	public void testEncodeString_generatePasswordsWithEqualSalts() {
		when(saltGenerator.generateSalt(32)).thenReturn(salt);
		Password password = encoder.encode(passwordString);
		Password otherPassword = encoder.encode(passwordString);
		assertTrue(password.equals(otherPassword));
		verify(saltGenerator, times(2)).generateSalt(32);
	}
	
	@Test
	public void testEncodeString_generateDifferentPasswords() {
		when(saltGenerator.generateSalt(32)).thenReturn(salt);
		Password password = encoder.encode(passwordString);
		Password otherPassword = encoder.encode(otherPasswordString);
		assertFalse(password.equals(otherPassword));
		verify(saltGenerator, times(2)).generateSalt(32);		
	}

	@Test
	public void testEncodeStringByteArray_equalPasswords() {
		when(saltGenerator.generateSalt(32)).thenReturn(salt);
		Password password = encoder.encode(passwordString);
		Password otherPassword = encoder.encode(passwordString, password.getSalt());
		assertTrue(password.equals(otherPassword));		
		verify(saltGenerator, times(1)).generateSalt(32);
	}

	@Test
	public void testEncodeStringByteArray_differentPasswords() {
		when(saltGenerator.generateSalt(32)).thenReturn(salt);
		Password password = encoder.encode(passwordString);
		Password otherPassword = encoder.encode(otherPasswordString, password.getSalt());
		assertFalse(password.equals(otherPassword));	
		verify(saltGenerator, times(1)).generateSalt(32);	
	}

}
